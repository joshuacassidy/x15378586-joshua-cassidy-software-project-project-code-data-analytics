from scipy import stats
import pymongo
import PyNonpar
from PyNonpar import*
import csv

client = pymongo.MongoClient("mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/",authSource="thesis")

raw_results = client["thesis"]['predicted_networks'].find({ "labels": {"$ne": None} })


data_labels = {}
data = []

for i in raw_results:
    labels = i['labels']
    labels.sort()
    if ''.join(labels) not in data_labels:
        data_labels[''.join(labels)] = labels
        data.append(i)
    

gen_data = []
gen_data_labels = {}
for i in client["thesis"]['best_generated_networks'].find({ } , sort=[('acc', -1)]):
    
    if 'labels' not in i or i['labels'] is None:
        continue
    labels = i['labels']
    labels.sort()
    if ''.join(i['labels']) in gen_data_labels:
        continue
    if ''.join(labels) in data_labels:
        gen_data_labels[''.join(labels)] = labels
        gen_data.append(i)

gen_data.sort(key=lambda x: "".join(sorted(x['labels'])))
data.sort(key=lambda x: "".join(sorted(x['labels'])))

generator_data_accuracies = [i['acc']*100 for i in gen_data]
predictor_data_accuracies = [i['acc']*100 for i in data]

print('generator accuracy shapiro test', stats.shapiro(generator_data_accuracies))
print('predictor accuracy shapiro test', stats.shapiro(predictor_data_accuracies))



import PyNonpar
from PyNonpar import*


print(PyNonpar.twosample.wilcoxon_mann_whitney_test(generator_data_accuracies, predictor_data_accuracies, alternative="less", method = "asymptotic", alpha = 0.01))
print(PyNonpar.twosample.wilcoxon_mann_whitney_test(generator_data_accuracies, predictor_data_accuracies, alternative="greater", method = "asymptotic", alpha = 0.01))
print(PyNonpar.twosample.wilcoxon_mann_whitney_test(generator_data_accuracies, predictor_data_accuracies, alternative="two.sided", method = "asymptotic", alpha = 0.01))


generator_data_losses = [i['loss'] for i in gen_data]
predictor_data_losses = [i['loss'] for i in data]

print('generator loss shapiro test', stats.shapiro(generator_data_losses))
print('predictor loss shapiro test', stats.shapiro(predictor_data_losses))


with open('losses.csv', "w") as f:

    fieldnames = ['generator_loss', 'predictor_loss']
    writer = csv.DictWriter(f, fieldnames=fieldnames)
    writer.writeheader()
    
    for i in range(len(generator_data_losses)):
        writer.writerow({'generator_loss': generator_data_losses[i], 'predictor_loss': predictor_data_losses[i]})

with open('accuracies.csv', "w") as f:
    fieldnames = ['generator_accuracy', 'predictor_accuracy']
    writer = csv.DictWriter(f, fieldnames=fieldnames)
    writer.writeheader()
    for i in range(len(generator_data_accuracies)):
        writer.writerow({'generator_accuracy': generator_data_accuracies[i]*100, 'predictor_accuracy': predictor_data_accuracies[i]*100})



print(PyNonpar.twosample.wilcoxon_mann_whitney_test(generator_data_losses, predictor_data_losses, alternative="less", method = "asymptotic", alpha = 0.01))
print(PyNonpar.twosample.wilcoxon_mann_whitney_test(generator_data_losses, predictor_data_losses, alternative="greater", method = "asymptotic", alpha = 0.01))
print(PyNonpar.twosample.wilcoxon_mann_whitney_test(generator_data_losses, predictor_data_losses, alternative="two.sided", method = "asymptotic", alpha = 0.01))



import random
import numpy
from matplotlib import pyplot
pyplot.style.use('seaborn-deep')

bins = numpy.linspace(0, 1, 11)
pyplot.hist([generator_data_losses, predictor_data_losses], bins, label=['Generator Loss', 'Predictor Loss'])
pyplot.legend(loc='upper right')
pyplot.xticks(bins)
pyplot.title("Generator Loss VS Predictor Loss")
pyplot.xlabel("Loss")
pyplot.ylabel("Count of Models")

pyplot.show()




pyplot.style.use('seaborn-deep')

bins = numpy.linspace(0, 100, 11)
pyplot.hist([generator_data_accuracies, predictor_data_accuracies], bins, label=['Generator Accuracy', 'Predictor Accuracy'])
pyplot.legend(loc='upper left')
pyplot.xticks(bins)
pyplot.title("Generator Accuracy VS Predictor Accuracy")
pyplot.xlabel("Accuracy%")
pyplot.ylabel("Count of Models")

pyplot.show()