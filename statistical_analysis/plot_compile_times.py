import pymongo
import numpy
from matplotlib import pyplot

client = pymongo.MongoClient("mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/",authSource="thesis")

raw_results = client["thesis"]['predicted_networks'].find({ "labels": {"$ne": None} })


predictor_data_labels = {}
predictor_data = []

for i in raw_results:
    labels = i['labels']
    labels.sort()
    if ''.join(labels) not in predictor_data_labels:
        predictor_data_labels[''.join(labels)] = labels
        predictor_data.append(i)
    
generator_data = {}
for i in client["thesis"]['all_generated_networks'].find({ }):
    if 'labels' not in i or i['labels'] is None:
        continue
    labels = i['labels']
    labels.sort()
    if ''.join(i['labels']) not in predictor_data_labels:
        continue
    if ''.join(i['labels']) not in generator_data:
        generator_data[''.join(i['labels'])] = { 'labels': labels, 'compile_time': i['compile_time']}
    else:
        generator_data[''.join(i['labels'])]['compile_time'] += i['compile_time']
    
generator_data = list(generator_data.values())
generator_data.sort(key=lambda x: "".join(sorted(x['labels'])))

predictor_data.sort(key=lambda x: "".join(sorted(x['labels'])))


predictor_data = [i['compile_time'] for i in predictor_data]
generator_data = [i['compile_time'] for i in generator_data]


pyplot.style.use('seaborn-deep')

bins = numpy.linspace(0, max(generator_data))
pyplot.hist(generator_data, bins, label=['Generator Compile Time'])
pyplot.legend(loc='upper right')
pyplot.title("Generator Compile Time")
pyplot.xlabel("Compile Time (in Seconds)")
pyplot.ylabel("Count of Models")

pyplot.show()


bins = numpy.linspace(0, max(predictor_data))
pyplot.hist(predictor_data, bins, label=['Predictor Compile Time'])
pyplot.legend(loc='upper right')
pyplot.title("Predictor Compile Time")
pyplot.xlabel("Compile Time (in Seconds)")
pyplot.ylabel("Count of Models")


pyplot.show()