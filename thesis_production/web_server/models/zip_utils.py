import hashlib, os

def save_dataset_zip(uploaded_file, upload_dir):
    file_contents = uploaded_file.read()
    file_name = hashlib.sha1(file_contents).hexdigest()
    save_directory = os.path.join(upload_dir, str(file_name) +'.zip')
    if not os.path.isdir(save_directory):
        with open(save_directory, 'wb') as image_zip:
            image_zip.write(file_contents)
    return file_name
