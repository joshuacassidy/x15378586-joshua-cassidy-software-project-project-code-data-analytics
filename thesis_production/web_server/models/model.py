import subprocess, os, pymongo
from io import BytesIO
from PIL import Image
from skimage import io
import os
from io import BytesIO
import zipfile, pymongo, shutil
from bson.json_util import dumps

upload_dir = "upload_directory"

def generate_model_job(file_name, unstructured_data_path, email, base_url, mutation_rate = None, crossover_rate = None, early_stopping = None, generations = None, population_size = None):
    bashCommand = "python3 create_generator_model.py {upload_dir} {file_name} {unstructured_data_path} {base_url} {email} {mutation_rate} {crossover_rate} {early_stopping} {generations} {population_size}".format(
        upload_dir=upload_dir, file_name=file_name, unstructured_data_path=unstructured_data_path, 
        base_url=base_url, email=email, mutation_rate=str(mutation_rate), crossover_rate=str(crossover_rate), 
        early_stopping=str(early_stopping), generations=str(generations), population_size=str(population_size))
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    return bashCommand

def predict_model_job(file_name, unstructured_data_path, email, base_url):
    bashCommand = "python3 create_predictor_model.py {upload_dir} {file_name} {unstructured_data_path} {base_url} {email}".format(upload_dir=upload_dir, file_name=file_name, unstructured_data_path=unstructured_data_path, base_url=base_url, email=email)
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    return bashCommand

def get_best_model(filename, file_type, models_folder):
    models_directory = os.listdir(os.path.join(upload_dir, str(filename) + models_folder))
    best = None
    best_model = None
    for model in models_directory:
        if not model.startswith(".") and model.endswith(file_type):
            current_model = model.split(file_type)[0]
            metric = [float(i) for i in current_model.split(":")][0]
            if best is None or best < metric:
                best = metric
                best_model = model
    return best_model

def get_image(filename, models_folder):
    best_model = get_best_model(filename, ".png", models_folder)
    img = Image.fromarray(io.imread(os.path.join(upload_dir, str(filename) + models_folder, best_model)))
    file_object = BytesIO()
    img.save(file_object, "PNG")
    file_object.seek(0)
    return file_object

def get_model_json(model_hash, model_collection, metric):
    client = pymongo.MongoClient(
        "mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/?compressors=disabled&gssapiServiceName=mongodb",
        authSource="thesis"
    )
    record = client["thesis"][model_collection].find_one({"image_set_hash": model_hash}, sort=[(metric, -1)])
    client.close()
    return record

def raw_model_zipfile(model_hash, file_path):
    memory_file = BytesIO()
    with zipfile.ZipFile(memory_file, "w") as zf:
        zf.write("web_server/static/instructions/instructions.py", "instructions.py")
        zf.write(file_path, "model.h5")
        
        path = "upload_directory/{model_hash}_structured/train".format(model_hash=model_hash)
        for i in range(2):
            for file in os.listdir(path):
                if not file.startswith("."):
                    path = os.path.join(path, file)
                    break
        
        zf.write(path, "demo."+ path.split(".")[-1])

    memory_file.seek(0)
    return memory_file

def json_model_zipfile(model_hash, collection_name, metric):
    client = pymongo.MongoClient("mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/",authSource="thesis")
    record = client["thesis"][collection_name].find_one({metric: {"$exists": True}}, sort=[(metric, -1)])["arch"]
    client.close()
    
    memory_file = BytesIO()
    with zipfile.ZipFile(memory_file, "w") as zf:
        zf.writestr("model.json", dumps(record))
        zf.write("web_server/static/instructions/instructions_json.py", "instructions_json.py")

        path = "upload_directory/{model_hash}_structured".format(model_hash=model_hash)

        if "{model_hash}_structured_instructions.zip".format(model_hash=model_hash) not in os.listdir("upload_directory"):
            shutil.make_archive(
                path + "_instructions", 
                "zip", 
                path
            )
        
        zf.write(path + "_instructions.zip", "{model_hash}_structured_instructions.zip".format(model_hash=model_hash))

    memory_file.seek(0)
    return memory_file
