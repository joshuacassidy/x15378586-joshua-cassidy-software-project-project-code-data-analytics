import re


def validate_email(request):
    regex = re.compile("(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
    email_error = None
    if ('email' not in request or request['email'] is None or request['email'].strip() == ''):
        email_error = "Please enter a email"
    elif regex.match(request['email']) is None:
        email_error = "Please enter a valid email"
    
    return email_error

def vailidate_zip(files):
    if files is None or 'zip' not in files or files['zip'] is None or files['zip'].filename.strip() == '':
        return "Please upload a zip file"
    else: 
        return None

def validate_mutation_rate(request):
    mutation = "The Mutation Rate needs to be between 1 and 0 for the genetic algorithms process"
    try:
        if 'mutation_rate' in request and (float(request['mutation_rate']) > 1 or float(request['mutation_rate']) < 0):
            return mutation
    except:
        return mutation
    return None

def validate_crossover_rate(request):
    crossover_error = "The Crossover Rate needs to be between 1 and 0 for the genetic algorithms process"
    try:
        if 'crossover_rate' in request and (float(request['crossover_rate']) > 1 or float(request['crossover_rate']) < 0):
                return crossover_error
    except:
        return crossover_error

    return None

def validate_population_size(request):
    population_size = "The genetic algorithms process must have a population size of atleast 3"
    try:
        if 'population_size' in request and (int(request['population_size']) < 3):
                return population_size
    except:
        return population_size
    
    return None

def validate_generations(request):
    generations = "The genetic algorithms process must run for atleast 1 generation"
    try:
        if 'generations' in request and (int(request['generations']) < 1):
            return generations
    except:
        return generations
    
    return None




