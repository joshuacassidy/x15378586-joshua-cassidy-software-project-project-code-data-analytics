import os, requests, json
from requests import Request, Session
from flask_cors import CORS
from flask import Flask, Response, request,render_template

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
    )

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    CORS(app, resources={r"/*": {"origins": "*"}})


    import web_server.controllers.instructions as instructions
    app.register_blueprint(instructions.bp)
    
    import web_server.controllers.generate_model as generate_model
    app.register_blueprint(generate_model.bp)

    import web_server.controllers.predicted_model as predicted_model
    app.register_blueprint(predicted_model.bp)

    import web_server.controllers.get_model as get_model
    app.register_blueprint(get_model.bp)

    import web_server.controllers.model_raw as model_raw
    app.register_blueprint(model_raw.bp)

    import web_server.controllers.model_json as model_json
    app.register_blueprint(model_json.bp)

    return app