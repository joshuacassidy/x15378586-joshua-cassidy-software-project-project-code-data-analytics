from flask import Blueprint
from flask import request
from flask import render_template, url_for, send_file
import os
from io import BytesIO
import zipfile, pymongo, shutil
from bson.json_util import dumps
import web_server.models.model as model

bp = Blueprint("instructions", __name__, url_prefix="/instructions")

@bp.route('/', methods=['GET'])
def index():
    url_for('static', filename='help.png')
    return render_template('instructions.html')

upload_dir = model.upload_dir

@bp.route('/raw/generated/<model_hash>', methods=['GET'])
def generated_raw(model_hash):
    try:
        best_model = model.get_best_model(model_hash, '.h5', '_structured_models')
        
        file_path = os.path.join(
            upload_dir, 
            str(model_hash) + '_structured_models', 
            best_model
        )

        memory_file = model.raw_model_zipfile(model_hash, file_path)
        return send_file(
            memory_file, 
            attachment_filename='demo.zip', 
            as_attachment=True
        )
    except FileNotFoundError as e:
        return render_template('not_found.html'), 404

@bp.route('/raw/predicted/<model_hash>', methods=['GET'])
def predicted_raw(model_hash):
    try:
        best_model = model.get_best_model(model_hash, '.h5', '_structured_models_pred')
        file_path = os.path.join(
            upload_dir, 
            str(model_hash) + '_structured_models_pred', 
            best_model
        )
        memory_file = model.raw_model_zipfile(model_hash, file_path)
        
        return send_file(
            memory_file, 
            attachment_filename='demo.zip', 
            as_attachment=True
        )
    except FileNotFoundError:
        return render_template('not_found.html'), 404

@bp.route('/json/generated/<model_hash>', methods=['GET'])
def generated_json(model_hash):
    try:
        memory_file = model.json_model_zipfile(model_hash, 'generated_network_arch', 'fitness')
        return send_file(memory_file, attachment_filename='demo.zip', as_attachment=True)
    except FileNotFoundError:
        return render_template('not_found.html'), 404


@bp.route('/json/predicted/<model_hash>', methods=['GET'])
def predicted_json(model_hash):
    try:
        memory_file = model.json_model_zipfile(model_hash, 'predicted_network_arch', 'acc')
        return send_file(memory_file, attachment_filename='demo.zip', as_attachment=True)
    except FileNotFoundError:
        return render_template('not_found.html'), 404

