from flask import Flask, Response, request, send_file, send_from_directory
import json
from bson.json_util import dumps
import hashlib
import zipfile, os, shutil
from distutils.dir_util import copy_tree
import random
import threading, pymongo, smtplib, re
from flask_cors import CORS
from flask import render_template
from flask import abort, redirect, url_for
import io, zipfile, time
import numpy as np
from PIL import Image
from skimage import io
from io import BytesIO
from flask import Blueprint
import web_server.models.model as model

bp = Blueprint("raw", __name__, url_prefix="/raw")


upload_dir = os.path.join(os.getcwd(), model.upload_dir)


@bp.route('/generated/<model_hash>', methods=['GET'])
def get_generated_model(model_hash):
    try:
        best_model = model.get_best_model(model_hash, '.h5', '_structured_models')

        result = send_file(os.path.join(upload_dir, str(model_hash) + '_structured_models', best_model), attachment_filename='model.h5', as_attachment=True)
        return result
    except FileNotFoundError as e:
        return render_template('not_found.html'), 404


@bp.route('/predicted/<model_hash>', methods=['GET', 'POST'])
def get_predict_model(model_hash):
    try:
        best_model = model.get_best_model(model_hash, '.h5', '_structured_models_pred')
        
        result = send_file(os.path.join(upload_dir, str(model_hash) + '_structured_models_pred', best_model), attachment_filename='model.h5', as_attachment=True)
        return result
    except FileNotFoundError:
        return render_template('not_found.html'), 404