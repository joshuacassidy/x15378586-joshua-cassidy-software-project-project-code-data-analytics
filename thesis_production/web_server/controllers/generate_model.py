from flask import Flask, Response, request, send_file
import json, hashlib
from flask_cors import CORS
import subprocess, zipfile, re, os

from flask import Blueprint
from flask import request, render_template, url_for
import web_server.models.vaildation as vaildation
import web_server.models.model as model
import web_server.models.zip_utils as zip_utils


bp = Blueprint("generated", __name__, url_prefix="/")

upload_dir = model.upload_dir

@bp.route('/', methods=['GET'])
@bp.route('/generate', methods=['GET'])
def render_generate_model(endpoint="generate_model", title="Generate Network Architecture"):
    return render_template('index.html', endpoint=endpoint, title=title)

@bp.route('/generate_custom', methods=['GET'])
def render_generate_model_custom(endpoint="generate_model_custom", title="Generate Network Architecture"):
    return render_template('custom_model_generate.html', endpoint=endpoint, title=title)


@bp.route('/api/generate_model', methods=['POST'])
def generate_model_api():
    try:       
        email_error = vaildation.validate_email(request.form)
        zip_error = vaildation.vailidate_zip(request.files)
        mutation_rate_error = vaildation.validate_mutation_rate(request.form)
        crossover_rate_error = vaildation.validate_crossover_rate(request.form)
        generations_error = vaildation.validate_generations(request.form)
        population_size_error = vaildation.validate_population_size(request.form)

        


        if email_error:
            response = json.dumps({"message": email_error})
            return Response(response, mimetype='application/json', status='400')
        elif zip_error: 
            response = json.dumps({"message": zip_error})
            return Response(response, mimetype='application/json', status='400')
        elif mutation_rate_error:
            response = json.dumps({"message": mutation_rate_error})
            return Response(response, mimetype='application/json', status='400')
        elif crossover_rate_error:
            response = json.dumps({"message": crossover_rate_error})
            return Response(response, mimetype='application/json', status='400')
        elif generations_error:
            response = json.dumps({"message": generations_error})
            return Response(response, mimetype='application/json', status='400')
        elif population_size_error:
            response = json.dumps({"message": population_size_error})
            return Response(response, mimetype='application/json', status='400')
        else: 
            email = request.form['email']
            uploaded_file = request.files['zip']
            mutation_rate = request.form['mutation_rate'] if 'mutation_rate' in request.form else None
            crossover_rate = request.form['crossover_rate'] if 'crossover_rate' in request.form else None
            early_stopping = request.form['early_stopping'] if 'early_stopping' in request.form else None
            generations = request.form['generations'] if 'generations' in request.form else None
            population_size = request.form['population_size'] if 'population_size' in request.form else None

            file_name = zip_utils.save_dataset_zip(uploaded_file, upload_dir)
            unstructured_data_path = os.path.join(upload_dir, str(file_name))
            model.generate_model_job(file_name, unstructured_data_path, email, request.url_root, mutation_rate, crossover_rate, early_stopping, generations, population_size)
            
            response = json.dumps({"message": "An email will be sent to the provided email address with details to download the generated model"})
            return Response(response, mimetype='application/json', status='200')
    except zipfile.BadZipFile:
        response = json.dumps({"message": "Error the file uploaded is not a zip file"})
        return Response(response, mimetype='application/json', status='400')

@bp.route('/generate_model', methods=['POST'])
def generate_model(email_error=None, zip_error=None, endpoint="generate_model", title="Generate Network Architecture"):
    try:
        email_error = vaildation.validate_email(request.form)
        zip_error = vaildation.vailidate_zip(request.files)
        
        if email_error or zip_error:
            return render_template('index.html', email_error=email_error, zip_error=zip_error, endpoint=endpoint, title=title)
        else: 
            email = request.form['email']
            uploaded_file = request.files['zip']
            upload_dir = 'upload_directory'
            file_name = zip_utils.save_dataset_zip(uploaded_file, upload_dir)
            unstructured_data_path = os.path.join(upload_dir, str(file_name))
            
            model.generate_model_job(file_name, unstructured_data_path, email, request.url_root)
            
            return render_template('thank_you.html')
    except zipfile.BadZipFile:
        return render_template('error.html', error = 'File uploaded is not a zip file')

@bp.route('/generate_model_custom', methods=['POST'])
def generate_model_custom(email_error=None, zip_error=None, endpoint="generate_model_custom", title="Generate Network Architecture"):
    try:
        email_error = vaildation.validate_email(request.form)
        zip_error = vaildation.vailidate_zip(request.files)
        mutation_rate_error = vaildation.validate_mutation_rate(request.form)
        crossover_rate_error = vaildation.validate_crossover_rate(request.form)
        generations_error = vaildation.validate_generations(request.form)
        population_size_error = vaildation.validate_population_size(request.form)
        
        if email_error or zip_error or mutation_rate_error or crossover_rate_error or generations_error or population_size_error:
            return render_template(
                'custom_model_generate.html', 
                endpoint=endpoint, 
                title=title,
                email_error=email_error,
                zip_error=zip_error,
                mutation_rate_error=mutation_rate_error,
                crossover_rate_error=crossover_rate_error,
                generations_error=generations_error,
                population_size_error=population_size_error
            )
        else: 
            email = request.form['email']
            uploaded_file = request.files['zip']
            mutation_rate = request.form['mutation_rate'] if 'mutation_rate' in request.form else None
            crossover_rate = request.form['crossover_rate'] if 'crossover_rate' in request.form else None
            early_stopping = request.form['early_stopping'] if 'early_stopping' in request.form else None
            generations = request.form['generations'] if 'generations' in request.form else None
            population_size = request.form['population_size'] if 'population_size' in request.form else None
            upload_dir = 'upload_directory'
            file_name = zip_utils.save_dataset_zip(uploaded_file, upload_dir)
            unstructured_data_path = os.path.join(upload_dir, str(file_name))
            
            model.generate_model_job(file_name, unstructured_data_path, email, request.url_root, mutation_rate, crossover_rate, early_stopping, generations, population_size)
            
            return render_template('thank_you.html')
    except zipfile.BadZipFile:
        return render_template('error.html', error = 'File uploaded is not a zip file')
