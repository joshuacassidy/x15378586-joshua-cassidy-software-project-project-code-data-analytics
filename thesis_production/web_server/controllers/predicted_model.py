from flask import Flask, Response, request, send_file
import json, hashlib
from flask_cors import CORS
import subprocess, zipfile, re, os

from flask import Blueprint
from flask import request, render_template, url_for
import web_server.models.vaildation as vaildation
import web_server.models.model as model
import web_server.models.zip_utils as zip_utils

bp = Blueprint("predicted", __name__, url_prefix="/")

upload_dir = model.upload_dir

@bp.route('/predict', methods=['GET'])
def render_predict_model(endpoint="predict_model", title="Predict Network Architecture"):
    return render_template('index.html', endpoint=endpoint, title=title)

@bp.route('/api/predict_model', methods=['POST'])
def predict_model():
    try:
        email_error = vaildation.validate_email(request.form)
        zip_error = vaildation.vailidate_zip(request.files)
        
        if email_error:
            response = json.dumps({"message": email_error})
            return Response(response, mimetype='application/json', status='400')
        elif zip_error: 
            response = json.dumps({"message": zip_error})
            return Response(response, mimetype='application/json', status='400')
        else: 
            email = request.form['email']
            uploaded_file = request.files['zip']
            file_name = zip_utils.save_dataset_zip(uploaded_file, upload_dir)
            
            unstructured_data_path = os.path.join(upload_dir, str(file_name))
            
            model.predict_model_job(
                file_name=file_name, 
                unstructured_data_path=unstructured_data_path, 
                email=email, 
                base_url=request.url_root,
            )
            response = json.dumps({"message": "An email will be sent to the provided email address with details to download the predicted model"})
            return Response(response, mimetype='application/json', status='200')
    except zipfile.BadZipFile:
        response = json.dumps({"message": "Error the file uploaded is not a zip file"})
        return Response(response, mimetype='application/json', status='400')


@bp.route('/predict_model', methods=['POST'])
def predict_arch(email_error=None, zip_error=None, endpoint="predict_model", title="Predict Network Architecture"):
    try:
        email_error = vaildation.validate_email(request.form)
        zip_error = vaildation.vailidate_zip(request.files)
        
        if email_error or zip_error:
            return render_template('index.html', email_error=email_error, zip_error=zip_error, endpoint=endpoint, title=title)
        else: 
            email = request.form['email']
            uploaded_file = request.files['zip']
            file_name = zip_utils.save_dataset_zip(uploaded_file, upload_dir)
            
            unstructured_data_path = os.path.join(upload_dir, str(file_name))
            model.predict_model_job(file_name, unstructured_data_path, email, request.url_root)
            
            return render_template('thank_you.html')
    except zipfile.BadZipFile:
        return render_template('error.html', error = 'File uploaded is not a zip file')
