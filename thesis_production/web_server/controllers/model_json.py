from flask import Flask, Response, request, send_file, send_from_directory
import json
from bson.json_util import dumps
import hashlib
import zipfile, os, shutil
from distutils.dir_util import copy_tree
import random
import threading, pymongo, smtplib, re
from flask_cors import CORS
from flask import render_template
from flask import abort, redirect, url_for
import io, zipfile, time
import numpy as np
from PIL import Image
from skimage import io
from io import BytesIO
from flask import Blueprint
import web_server.models.model as model

bp = Blueprint("json", __name__, url_prefix="/")

upload_dir = model.upload_dir

@bp.route('/json/generated/<model_hash>', methods=['GET', 'POST'])
def generated_json_model_download(model_hash):
    
    record = model.get_model_json(model_hash, 'best_generated_networks', "fitness")
    if record is not None:
        return Response(dumps(record), mimetype='application/json', status='200', headers={"Content-Disposition": "attachment;filename=json_architecture.json"})
    else:
        return render_template('not_found.html'), 404

@bp.route('/json/predicted/<model_hash>', methods=['GET', 'POST'])
def predict_json_model_download(model_hash):
    record = model.get_model_json(model_hash, 'predicted_networks', "acc")
    if record is not None:
        return Response(dumps(record), mimetype='application/json', status='200', headers={"Content-Disposition": "attachment;filename=json_architecture.json"})
    else:
        return render_template('not_found.html'), 404







