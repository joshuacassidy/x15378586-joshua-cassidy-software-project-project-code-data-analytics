from flask import Flask, Response, request, send_file
import json, hashlib
from flask_cors import CORS
import subprocess, zipfile, re, os
from skimage import io
from PIL import Image
from io import BytesIO
from flask import Blueprint
from flask import request, render_template, url_for
import web_server.models.model as model
import pymongo

bp = Blueprint("model", __name__, url_prefix="/")

upload_dir = model.upload_dir

@bp.route('/generated/<model_hash>', methods=['GET'])
def render_download_model_generated(model_hash):
    files_dir = os.path.join(upload_dir, str(model_hash) + '_structured_models')
    if os.path.isdir(files_dir) and len(os.listdir(files_dir)) > 0:
        
        record = model.get_model_json(model_hash, 'best_generated_networks', "fitness")

        return render_template(
            'download_arch.html', 
            raw_download_link="/raw/generated/" + model_hash, 
            json_download_link = "/json/generated/" + model_hash,
            instructions = "/instructions/raw/generated/" + model_hash,
            instructions_json = "/instructions/json/generated/" + model_hash,
            arch = "/generated_image/" + model_hash,
            loss = round(record['loss'],2),
            acc = round(record['acc']*100,2)
        )
    else:
        return render_template('not_found.html')

@bp.route('/predicted/<model_hash>', methods=['GET'])
def render_download_model_predicted(model_hash):
    files_dir = os.path.join(upload_dir, str(model_hash) + '_structured_models_pred')
    if os.path.isdir(files_dir) and len(os.listdir(files_dir)) > 0:

        record = model.get_model_json(model_hash, 'predicted_networks', "acc")
        
        return render_template(
            'download_arch.html', 
            raw_download_link="/raw/predicted/" + model_hash, 
            json_download_link = "/json/predicted/" + model_hash,
            instructions = "/instructions/raw/predicted/" + model_hash,
            instructions_json = "/instructions/json/predicted/" + model_hash,
            arch = "/predicted_image/" + model_hash,
            loss = round(record['loss'],2),
            acc = round(record['acc']*100,2)
        )
    else:
        return render_template('not_found.html')
