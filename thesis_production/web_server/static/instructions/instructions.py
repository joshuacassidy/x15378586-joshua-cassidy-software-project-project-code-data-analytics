# Note for more information on how to use the model: https://keras.io/
from keras.models import load_model
import pathlib, os
from keras.preprocessing import image
import numpy as np
from keras.utils import plot_model

# Load the Keras Convolutional Neural Network Configuration
os.chdir(pathlib.Path(__file__).parent.absolute())
model = load_model('model.h5')

# Load the demonstrative Image To Be Classified
img = image.load_img([i for i in os.listdir() if i.startswith('demo.') ][0], target_size=(32, 32))
img = image.img_to_array(img)
img = np.expand_dims(img, axis=0)

# Classify the loaded image
print(model.predict_classes(img))

# Visualise the Model
plot_model(model, to_file="model.png", show_layer_names=False, show_shapes=True)
