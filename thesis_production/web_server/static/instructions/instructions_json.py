# Note for more information on how to use the model: https://keras.io/
import matplotlib.image as mpimg
from zipfile import ZipFile
import os
import pathlib
import json
os.chdir(pathlib.Path(__file__).parent.absolute())
from keras.preprocessing.image import ImageDataGenerator
import keras
from keras.models import model_from_json
from keras.utils import plot_model

# Load the Keras Convolutional Neural Network Configuration
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(json.loads(loaded_model_json))

# Load the training data
data_dir = [i for i in os.listdir() if i.endswith('.zip')][0]

try:
    with ZipFile(data_dir, 'r') as zipObj:
        zipObj.extractall('data')
except:
    pass
data_dir = 'data'

train_datagen = ImageDataGenerator(
    rescale=1./255,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True
)

test_datagen = ImageDataGenerator(rescale=1./255)
validation_datagen = ImageDataGenerator(rescale=1./255)

img_dir = os.path.join(data_dir, 'train')

label = None
for i in os.listdir(img_dir):
    if not i.startswith("."):
        label = i
        break

for i in os.listdir(os.path.join(img_dir, label)):
    if not i.startswith("."):
        img = i
        break

os.listdir(os.path.join(data_dir, 'train'))

image_shape = mpimg.imread(os.path.join(img_dir, label, img)).shape

train_generator = train_datagen.flow_from_directory(
    os.path.join(data_dir, 'train'),
    target_size=(image_shape[0], image_shape[1]),
    class_mode='categorical'
)

validation_generator = train_datagen.flow_from_directory(
    os.path.join(data_dir, 'validation'),
    target_size=(image_shape[0], image_shape[1]),
    class_mode='categorical'
)

test_generator = test_datagen.flow_from_directory(
    os.path.join(data_dir, 'test'),
    target_size=(image_shape[0], image_shape[1]),
    class_mode='categorical'
)

# Compile the model
model.compile(
    loss=keras.losses.categorical_crossentropy,
    optimizer=keras.optimizers.Adadelta(),
    metrics=['accuracy']
)

# Train the model
history = model.fit_generator(
    train_generator,
    steps_per_epoch=5,
    epochs=1,
    validation_data=validation_generator,
    validation_steps=10,
    workers=12,
    max_queue_size=20,
    callbacks=[keras.callbacks.EarlyStopping(
        monitor='accuracy', mode='max', patience=2, restore_best_weights=True)]

)

# Evaluate the model
test_loss, test_acc = model.evaluate_generator(
    test_generator,
    steps=1,
    workers=5
)

# Visualise the Model
plot_model(model, to_file="model.png", show_layer_names=False, show_shapes=True)
