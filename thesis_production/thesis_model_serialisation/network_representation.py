
MAX_LAYERS = 7
LAYERS = len("111")
NODES = len("11111111")
WIDTH_KERNEL = len("11")
HEIGHT_KERNEL = len("11")
DEFAULT_DROPOUT_LAYER = [0, 0, 0, 0, 0, 0, 0]

def int_to_genes(integer, genes_size):
    binary_number = bin(integer).replace('0b', '')
    number_padding = ''.join(['0' for i in range(genes_size-len(binary_number))])
    return [str(i) for i in list(number_padding+binary_number)]

def genes_to_int(genes):
    genes_string = '0b' + (''.join(str(i) for i in genes))
    genes_int = int(genes_string, 2)
    return genes_int

def serialise(data): 
    layers = int_to_genes(data['layers'], LAYERS)
    nodes = [int_to_genes(i, NODES) for i in data['nodes']]
    dense_layers = int_to_genes(data['dense_layers'], LAYERS)
    dense_nodes = [int_to_genes(i, NODES) for i in data['dense_nodes']]
    dropouts = [int_to_genes(i, len(DEFAULT_DROPOUT_LAYER)) for i in data['dropouts']]
    kernel_width = [int_to_genes(i, WIDTH_KERNEL) for i in data['kernel_width']]
    kernel_height = [int_to_genes(i, HEIGHT_KERNEL) for i in data['kernel_height']]

    layers_str = "".join(layers)

    nodes_amount = MAX_LAYERS - len(nodes)
    blank_nodes = ''.join([''.join([str(0) for i in range(NODES)]) for j in range(nodes_amount)])
    nodes_str = blank_nodes + ''.join([''.join(i) for i in nodes])

    dense_layers = ''.join(dense_layers)

    dense_nodes_amount = MAX_LAYERS - len(dense_nodes)
    blank_dense_nodes = ''.join([''.join([str(0) for i in range(NODES)]) for j in range(dense_nodes_amount)])
    dense_nodes_amount = blank_dense_nodes + ''.join([''.join(i) for i in dense_nodes])

    kernel_amount = MAX_LAYERS - len(nodes)
    blank_kernel = ''.join([''.join([str(0) for i in range(WIDTH_KERNEL)]) for j in range(kernel_amount)])

    kernel_width_str = blank_kernel + ''.join([''.join(i) for i in kernel_width])
    kernel_height_str = blank_kernel + ''.join([''.join(i) for i in kernel_height])

    dropouts_amount = MAX_LAYERS - len(dropouts)
    blank_dropouts = ''.join([''.join([str(0) for i in range(len(DEFAULT_DROPOUT_LAYER))]) for j in range(dropouts_amount)])
    dropouts_str = blank_dropouts + ''.join([''.join(i) for i in dropouts])

    model_str = layers_str + dense_layers + nodes_str + dense_nodes_amount + kernel_width_str + kernel_height_str + dropouts_str
    
    model_serialisation = {
        'layers_bytes':  layers_str,
        'dense_layers_bytes':  dense_layers,
        'nodes_bytes':  nodes_str,
        'dense_nodes_bytes':  dense_nodes_amount,
        'kernel_width_bytes':  kernel_width_str,
        'kernel_height_bytes':  kernel_height_str,
        'dropouts_bytes':  dropouts_str,
        'model_representation':  model_str,
    }

    return model_serialisation

def extract_network_part(byte_str, max_number_layer, start_index, string_part):
  network_part = []
  for i in range(max_number_layer):
    end_index = start_index + string_part
    network_part.append(genes_to_int(byte_str[start_index: end_index]))
    start_index = end_index
  return (end_index, network_part)

def deserialise(byte_str):
  layers = genes_to_int(byte_str[:LAYERS])
  dense_layers = genes_to_int(byte_str[LAYERS: LAYERS * 2])

  start_index = (LAYERS * 2)
  start_index, nodes = extract_network_part(
    byte_str=byte_str,
    max_number_layer=MAX_LAYERS,
    start_index=start_index,
    string_part=NODES
  )
  
  start_index, dense_nodes = extract_network_part(
    byte_str=byte_str,
    max_number_layer=MAX_LAYERS,
    start_index=start_index,
    string_part=NODES
  )
  
  start_index, width_kernel = extract_network_part(
    byte_str=byte_str,
    max_number_layer=MAX_LAYERS,
    start_index=start_index,
    string_part=WIDTH_KERNEL
  )

  start_index, height_kernel = extract_network_part(
    byte_str=byte_str,
    max_number_layer=MAX_LAYERS,
    start_index=start_index,
    string_part=WIDTH_KERNEL
  )

  start_index, dropouts = extract_network_part(
    byte_str=byte_str,
    max_number_layer=MAX_LAYERS,
    start_index=start_index,
    string_part=len(DEFAULT_DROPOUT_LAYER)
  )

  return {
    "layers": layers,
    "dense_layers": dense_layers,
    "nodes": nodes,
    "dense_nodes": dense_nodes,
    "width_kernel": width_kernel,
    "height_kernel": height_kernel,
    "dropouts": dropouts,
    
  }

  
