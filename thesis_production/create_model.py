import zipfile, os, smtplib
import datetime, sys
import thesis_structure_data.structure_data as structure_data

def send_email(email, message, subject):
    smtp_server = "smtp.gmail.com"
    sender_email = "joshcassidythesis@gmail.com"
    password = "Josh123*"
    server = smtplib.SMTP("smtp.gmail.com:587")
    server.ehlo()
    server.starttls()
    message = """From: {msg_from}\nTo: {msg_to}\nSubject:{subject}\n\n{message}""".format(
        msg_to = email,
        msg_from = sender_email,
        message = message,
        subject = subject
    ) 
    server.login(sender_email, password)
    server.sendmail(sender_email, email, message)
    server.quit()


def create_generator_model(upload_dir, file_name, unstructured_data_path, base_url, email, mutation_rate, crossover_rate, early_stopping, generations, population_size):
    try:
        if mutation_rate.lower() == "none":
            mutation_rate = 0.0015
        if crossover_rate.lower() == "none":
            crossover_rate = 0.05
        if early_stopping.lower() == "true" or early_stopping.lower() == "on":
            early_stopping = True
        if generations.lower() == "none":
            generations = 10
        if population_size.lower() == "none":
            population_size = 20

        save_directory = os.path.join(upload_dir, str(file_name) +".zip")
        if not os.path.isdir(unstructured_data_path):
            with zipfile.ZipFile(save_directory, "r") as image_zip:
                image_zip.extractall(unstructured_data_path)    

        try: 
            os.mkdir(os.path.join(upload_dir, str(file_name) + "_structured_models" )) 
        except:
            pass
        
        structure_data.structure_data(unstructured_data_path, upload_dir, file_name)

        import final_year_project_thesis.app as thesis
        loss, acc = thesis.generate_networks(
            base_dir=os.path.join(upload_dir, str(file_name) + "_structured" ), 
            early_stopping=early_stopping, 
            population_size=int(population_size), 
            generations=int(generations), 
            crossover_rate=float(crossover_rate), 
            mutation_rate=float(mutation_rate)
        )
        
        message="""Your generated model has been successfully generated. Your architecture has an accuracy of {acc} and has a loss value of {loss}. Your generated network architecture had can be downloaded at {url}""".format(
            url=os.path.join(base_url,"generated", file_name),
            loss=round(loss,2),
            acc=round(acc*100, 2)
        )
        send_email(email, message, "Your generated model")
    except Exception as e:
        message="""An issue occured while preprocessing your dataset in the process of creating your generating your model architecture, please ensure that your dataset adheres to the guides that can be found at: {url}""".format(
        url=os.path.join(base_url,"instructions"),
    )
        send_email(email, message, "Your generated model")


def create_predictor_model(upload_dir, file_name, unstructured_data_path, base_url, email):
    import predictor_network.generate_network_arch as generate_network_arch
    import predictor_network.build_network as build_network
    import clean_uploaded_dataset.image_stats as image_stats
    
    try:
        save_directory = os.path.join(upload_dir, str(file_name) +".zip")
        if not os.path.isdir(save_directory):
            with zipfile.ZipFile(save_directory, "r") as image_zip:
                image_zip.extractall(unstructured_data_path)

        dataset_stats = image_stats.structure_data(unstructured_data_path, upload_dir, file_name)

        try: 
            os.mkdir(os.path.join(upload_dir, str(file_name) + "_structured_models_pred" )) 
        except:
            pass
        structure_data.structure_data(unstructured_data_path, upload_dir, file_name)
        model_str_rep = generate_network_arch.generate_network(dataset_stats)

        loss, acc = build_network.build_network(
            model_str_rep,
            unstructured_data_path + "_structured", 
            os.path.join(unstructured_data_path + "_structured_models_pred")
        )
        message="""Your predicted model has been successfully predicted. Your architecture has an accuracy of {acc} and has a loss value of {loss}. Your predicted network architecture had can be downloaded at {url}""".format(
            url=os.path.join(base_url,"predicted", file_name),
            loss=round(loss,2),
            acc=round(acc*100, 2)
        )   
        send_email(email, message, "Your predicted model")
    
        
    except Exception as e:
        message="""An issue occured while preprocessing your dataset in the process of creating your predicting your model architecture, please ensure that your dataset adheres to the guides that can be found at: {url}""".format(
        url=os.path.join(base_url,"instructions"),
    )
        send_email(email, message, "Your predicted model")
