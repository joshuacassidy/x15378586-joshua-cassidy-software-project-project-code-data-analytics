import sys
import create_model

input_file, upload_dir, file_name, unstructured_data_path, base_url, email, mutation_rate, crossover_rate, early_stopping, generations, population_size = sys.argv


create_model.create_generator_model(
    upload_dir=upload_dir,
    file_name=file_name,
    unstructured_data_path=unstructured_data_path,
    base_url=base_url,
    email=email,
    mutation_rate=mutation_rate,
    crossover_rate=crossover_rate, 
    early_stopping=early_stopping, 
    generations=generations, 
    population_size=population_size
)