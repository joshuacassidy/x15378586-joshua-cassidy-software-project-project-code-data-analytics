import unittest, requests

class TestGenerateNework(unittest.TestCase):

    def test_generate_network(self):
        payloads = [
            {"email": "joshcassidy79@gmail.com"},
            {
                "email": "joshcassidy79@gmail.com",
                "mutation_rate": 0.01,
                "crossover_rate": 0.4,
                "early_stopping": True,
                "generations": None,
                "population_size": 3,
            }
        ]
        for payload in payloads:
            url = "http://0.0.0.0:8080/api/generate_model"
            zip_file = open("zip.zip","rb")

            
            files = [
                ("zip",zip_file)
            ]

            response = requests.request("POST", url, data = payload, files = files)

            message = response.json()['message']
            self.assertEqual(response.status_code, 200)
            zip_file.close()
            response.close()
            self.assertEqual("An email will be sent to the provided email address with details to download the generated model", message)

    def test_generate_network_email_validation(self):

        input_email = ["joshcassidy79gmail.com", ""]
        expected_result = ["Please enter a valid email", "Please enter a email"]
        for i in range(2):
            url = "http://0.0.0.0:8080/api/generate_model"

            zip_file = open("zip.zip","rb")

            payload = {"email": input_email[i]}
            files = [
                ("zip", zip_file)
            ]

            response = requests.request("POST", url, data = payload, files = files)
            self.assertEqual(response.status_code, 400)
            message = response.json()['message']
            response.close()
            zip_file.close()

            self.assertEqual(message, expected_result[i])

    
    def test_generate_network_zipfile_validation(self):
        url = "http://0.0.0.0:8080/api/generate_model"

        payload = {"email": 'joshcassidy79@gmail.com'}
        files = [
            ("zip", '')
        ]

        response = requests.request("POST", url, data = payload, files = None)
        self.assertEqual(response.status_code, 400)
        message = response.json()['message']
        response.close()
        self.assertEqual(message, 'Please upload a zip file')

