import unittest, requests

class TestDownload(unittest.TestCase):

    def test_json_model(self):
        net_type = ['generated', 'predicted']

        for i in net_type:
            url = "http://0.0.0.0:8080/json/{net_type}/4d260e67032a85dea81d1bc7ed24256ddc90d03d".format(
                net_type = i
            )
            response = requests.request("GET", url)
            response_json = response.json()

            self.assertEqual('acc' in response_json, True)
            self.assertEqual('loss' in response_json, True)
            self.assertEqual(response.status_code, 200)
            response.close()
        
    def test_json_model_not_found(self):
        net_type = [
            'generated', 
            'predicted'
        ]

        for i in net_type:
            url = "http://0.0.0.0:8080/json/{net_type}/1".format(
                net_type = i
            )
            response = requests.request("GET", url)
            self.assertEqual(response.status_code, 404)
            response.close()


    def test_download_raw(self):
        net_type = ['generated', 'predicted']

        for i in net_type:
            url = "http://0.0.0.0:8080/raw/{net_type}/4d260e67032a85dea81d1bc7ed24256ddc90d03d".format(
                net_type = i
            )
            response = requests.request("GET", url)
            self.assertEqual(response.status_code, 200)
            response.close()
            
    def test_download_raw_not_found(self):
        instructions_type = ['generated', 'predicted']

        for i in instructions_type:
            url = "http://0.0.0.0:8080/raw/{instructions_type}/1".format(
                instructions_type = i
            )
            response = requests.request("GET", url)
            self.assertEqual(response.status_code, 404)
            response.close()

    def test_download_instructions(self):
        instructions_content_type = ['raw', 'json']
        instructions_type = ['generated', 'predicted']

        for i in instructions_content_type:
            for j in instructions_type:
                url = "http://0.0.0.0:8080/instructions/{content_type}/{instructions_type}/4d260e67032a85dea81d1bc7ed24256ddc90d03d".format(
                    content_type = i,
                    instructions_type = j
                )
                response = requests.request("GET", url)
                self.assertEqual(response.status_code, 200)
                response.close()

    def test_download_instruction_not_found(self):
        instructions_content_type = ['raw', 'json']
        instructions_type = ['generated', 'predicted']

        for i in instructions_content_type:
            for j in instructions_type:
                url = "http://0.0.0.0:8080/instructions/{content_type}/{instructions_type}/1".format(
                    content_type = i,
                    instructions_type = j
                )
                response = requests.request("GET", url)
                self.assertEqual(response.status_code, 404)
                response.close()


