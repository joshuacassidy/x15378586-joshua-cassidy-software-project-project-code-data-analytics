import unittest, requests

class TestPredictNework(unittest.TestCase):

    def test_predict_network(self):
        url = "http://0.0.0.0:8080/api/predict_model"

        zip_file = open("zip.zip","rb")

        payload = {"email": "joshcassidy79@gmail.com"}
        files = [
            ("zip", zip_file)
        ]

        response = requests.request("POST", url, data = payload, files = files)

        message = response.json()['message']
        response.close()
        zip_file.close()

        self.assertEqual("An email will be sent to the provided email address with details to download the predicted model", message)

    def test_predict_network_email_validation(self):

        input_email = ["joshcassidy79gmail.com", ""]
        expected_result = ["Please enter a valid email", "Please enter a email"]
        for i in range(2):
            url = "http://0.0.0.0:8080/api/predict_model"

            zip_file = open("zip.zip","rb")

            payload = {"email": input_email[i]}
            files = [
                ("zip", zip_file)
            ]

            response = requests.request("POST", url, data = payload, files = files)
            self.assertEqual(response.status_code, 400)
            message = response.json()['message']
            response.close()
            zip_file.close()

            self.assertEqual(message, expected_result[i])

    
    def test_predict_network_zipfile_validation(self):
        url = "http://0.0.0.0:8080/api/predict_model"

        payload = {"email": 'joshcassidy79@gmail.com'}
        files = [
            ("zip", '')
        ]

        response = requests.request("POST", url, data = payload, files = None)
        self.assertEqual(response.status_code, 400)
        message = response.json()['message']
        response.close()
        self.assertEqual(message, 'Please upload a zip file')
    
 