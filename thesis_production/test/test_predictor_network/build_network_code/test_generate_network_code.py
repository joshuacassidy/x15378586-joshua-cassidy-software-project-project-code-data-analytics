import unittest, random
from predictor_network import build_network

class TestGenerateNetworkCode(unittest.TestCase):

    def test_generate_network_code(self):
        expected_net_code="""
import datetime
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os, shutil

from keras import layers
from keras import models
from keras.utils import to_categorical
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import keras
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.image as mpimg

def list_files(path):
    return [f for f in os.listdir(path) if not f.startswith(".")]

def create_neural_net(train_dir, validation_dir, test_dir, output_directory, patience = 20):
    train_datagen = ImageDataGenerator(
            rescale=1./255,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True
        )
    
    validation_datagen = ImageDataGenerator(rescale=1./255)

    classes = len(list_files(train_dir))
    classification_folder_name = list_files(train_dir)[0]
    images_path = train_dir + "/" + classification_folder_name
    image_name = list_files(images_path)[0]
    image_path = train_dir + "/" + classification_folder_name + "/" + image_name

    image_shape = mpimg.imread(image_path).shape
    model = Sequential()
    model.add(Conv2D(64, kernel_size=(3, 3), 
                    activation="relu",
                    input_shape=(image_shape[0], image_shape[1], 3)))
    model.add(Dropout(0.11))
    model.add(Conv2D(121, (3, 1), activation="relu"))
    model.add(Conv2D(105, (2, 2), activation="relu"))
    model.add(Conv2D(56, (1, 3), activation="relu"))
    model.add(Conv2D(34, (2, 1), activation="relu"))
    model.add(Conv2D(21, (2, 2), activation="relu"))

    model.add(Flatten())
    model.add(Dense(12, activation="relu"))
    model.add(Dense(68, activation="relu"))
    
    model.add(Dense(classes, activation="softmax"))

    model.compile(
        loss=keras.losses.categorical_crossentropy,
        optimizer=keras.optimizers.Adadelta(),
        metrics=["accuracy"]
    )
    test_datagen = ImageDataGenerator(rescale=1./255)
    
    train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(image_shape[0], image_shape[1]),
        batch_size=50,
        class_mode="categorical"
    )

    validation_generator = train_datagen.flow_from_directory(
        validation_dir,
        target_size=(image_shape[0], image_shape[1]),
        batch_size=50,
        class_mode="categorical"
    )

    test_generator = test_datagen.flow_from_directory(
        test_dir,
        target_size=(image_shape[0], image_shape[1]),
        batch_size=50,
        class_mode="categorical"
    )


    history = model.fit_generator(
        train_generator, 
        steps_per_epoch=100, 
        epochs=40, 
        validation_data=validation_generator,
        validation_steps=20,
        workers=12,
        max_queue_size=20, 

    )

    

    test_loss, test_acc = model.evaluate_generator(
        test_generator, 
        steps=20,
        workers=5
    )

    base_model_string_rep = output_directory + "/" + str(test_acc) + ":" + str(test_loss)

    model.save(base_model_string_rep + ".h5")
    model_json = model.to_json()
    with open(base_model_string_rep + ".json", "w") as f:
        f.write(model_json)
    

    keras.backend.clear_session()

    return (test_loss, test_acc)
"""
        layers = 5
        dense_layers = 2
        nodes = [0, 0, 21, 34, 56, 105, 121]
        dense_nodes = [0, 0, 0, 0, 0, 68, 12]
        width_kernel = [0, 0, 2, 2, 1, 2, 3]
        height_kernel = [0, 0, 2, 1, 3, 2, 1]
        dropouts = [0, 0, 0, 0, 0, 0, 11]
        actual_net_code = build_network.generate_network_code(layers=layers, dense_layers=dense_layers, dense_nodes=dense_nodes, nodes=nodes, height_kernel=height_kernel, width_kernel=width_kernel, dropouts=dropouts)
        self.assertEqual(expected_net_code, actual_net_code)


    def test_generate_network_code_validate_incompatible_fields(self):
        expected_net_code="""
import datetime
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os, shutil

from keras import layers
from keras import models
from keras.utils import to_categorical
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import keras
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.image as mpimg

def list_files(path):
    return [f for f in os.listdir(path) if not f.startswith(".")]

def create_neural_net(train_dir, validation_dir, test_dir, output_directory, patience = 20):
    train_datagen = ImageDataGenerator(
            rescale=1./255,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True
        )
    
    validation_datagen = ImageDataGenerator(rescale=1./255)

    classes = len(list_files(train_dir))
    classification_folder_name = list_files(train_dir)[0]
    images_path = train_dir + "/" + classification_folder_name
    image_name = list_files(images_path)[0]
    image_path = train_dir + "/" + classification_folder_name + "/" + image_name

    image_shape = mpimg.imread(image_path).shape
    model = Sequential()
    model.add(Conv2D(64, kernel_size=(3, 3), 
                    activation="relu",
                    input_shape=(image_shape[0], image_shape[1], 3)))
    model.add(Dropout(1.0))
    model.add(Conv2D(121, (1, 1), activation="relu"))
    model.add(Dropout(0.06))
    model.add(Conv2D(105, (1, 1), activation="relu"))
    model.add(Dropout(0.05))
    model.add(Conv2D(56, (1, 1), activation="relu"))
    model.add(Dropout(0.04))
    model.add(Conv2D(34, (1, 1), activation="relu"))
    model.add(Dropout(0.03))
    model.add(Conv2D(45, (1, 1), activation="relu"))
    model.add(Dropout(0.02))
    model.add(Conv2D(1, (1, 1), activation="relu"))
    model.add(Dropout(0.01))
    model.add(Conv2D(4, (1, 1), activation="relu"))

    model.add(Flatten())
    model.add(Dense(1, activation="relu"))
    model.add(Dense(68, activation="relu"))
    model.add(Dense(1, activation="relu"))
    
    model.add(Dense(classes, activation="softmax"))

    model.compile(
        loss=keras.losses.categorical_crossentropy,
        optimizer=keras.optimizers.Adadelta(),
        metrics=["accuracy"]
    )
    test_datagen = ImageDataGenerator(rescale=1./255)
    
    train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(image_shape[0], image_shape[1]),
        batch_size=50,
        class_mode="categorical"
    )

    validation_generator = train_datagen.flow_from_directory(
        validation_dir,
        target_size=(image_shape[0], image_shape[1]),
        batch_size=50,
        class_mode="categorical"
    )

    test_generator = test_datagen.flow_from_directory(
        test_dir,
        target_size=(image_shape[0], image_shape[1]),
        batch_size=50,
        class_mode="categorical"
    )


    history = model.fit_generator(
        train_generator, 
        steps_per_epoch=100, 
        epochs=40, 
        validation_data=validation_generator,
        validation_steps=20,
        workers=12,
        max_queue_size=20, 

    )

    

    test_loss, test_acc = model.evaluate_generator(
        test_generator, 
        steps=20,
        workers=5
    )

    base_model_string_rep = output_directory + "/" + str(test_acc) + ":" + str(test_loss)

    model.save(base_model_string_rep + ".h5")
    model_json = model.to_json()
    with open(base_model_string_rep + ".json", "w") as f:
        f.write(model_json)
    

    keras.backend.clear_session()

    return (test_loss, test_acc)
"""
        layers = 7
        dense_layers = 3
        nodes = [4, 0, 45, 34, 56, 105, 121]
        dense_nodes = [0, 0, 0, 0, 0, 68, 0]
        width_kernel = [0, 0, 0, 0, 0, 0, 0]
        height_kernel = [0, 0, 0, 0, 0, 0, 0]
        dropouts = [1, 2, 3, 4, 5, 6, 101]
        actual_net_code = build_network.generate_network_code(layers=layers, dense_layers=dense_layers, dense_nodes=dense_nodes, nodes=nodes, height_kernel=height_kernel, width_kernel=width_kernel, dropouts=dropouts)
        self.assertEqual(expected_net_code, actual_net_code)
        


    def test_generate_network_code_mismatched_arrays(self):
        expected_net_code="""
import datetime
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os, shutil

from keras import layers
from keras import models
from keras.utils import to_categorical
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import keras
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.image as mpimg

def list_files(path):
    return [f for f in os.listdir(path) if not f.startswith(".")]

def create_neural_net(train_dir, validation_dir, test_dir, output_directory, patience = 20):
    train_datagen = ImageDataGenerator(
            rescale=1./255,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True
        )
    
    validation_datagen = ImageDataGenerator(rescale=1./255)

    classes = len(list_files(train_dir))
    classification_folder_name = list_files(train_dir)[0]
    images_path = train_dir + "/" + classification_folder_name
    image_name = list_files(images_path)[0]
    image_path = train_dir + "/" + classification_folder_name + "/" + image_name

    image_shape = mpimg.imread(image_path).shape
    model = Sequential()
    model.add(Conv2D(64, kernel_size=(3, 3), 
                    activation="relu",
                    input_shape=(image_shape[0], image_shape[1], 3)))
    model.add(Dropout(0.11))
    model.add(Conv2D(121, (3, 1), activation="relu"))
    model.add(Conv2D(105, (2, 2), activation="relu"))

    model.add(Flatten())
    model.add(Dense(12, activation="relu"))
    model.add(Dense(68, activation="relu"))
    model.add(Dense(12, activation="relu"))
    model.add(Dense(12, activation="relu"))
    
    model.add(Dense(classes, activation="softmax"))

    model.compile(
        loss=keras.losses.categorical_crossentropy,
        optimizer=keras.optimizers.Adadelta(),
        metrics=["accuracy"]
    )
    test_datagen = ImageDataGenerator(rescale=1./255)
    
    train_generator = train_datagen.flow_from_directory(
        train_dir,
        target_size=(image_shape[0], image_shape[1]),
        batch_size=50,
        class_mode="categorical"
    )

    validation_generator = train_datagen.flow_from_directory(
        validation_dir,
        target_size=(image_shape[0], image_shape[1]),
        batch_size=50,
        class_mode="categorical"
    )

    test_generator = test_datagen.flow_from_directory(
        test_dir,
        target_size=(image_shape[0], image_shape[1]),
        batch_size=50,
        class_mode="categorical"
    )


    history = model.fit_generator(
        train_generator, 
        steps_per_epoch=100, 
        epochs=40, 
        validation_data=validation_generator,
        validation_steps=20,
        workers=12,
        max_queue_size=20, 

    )

    

    test_loss, test_acc = model.evaluate_generator(
        test_generator, 
        steps=20,
        workers=5
    )

    base_model_string_rep = output_directory + "/" + str(test_acc) + ":" + str(test_loss)

    model.save(base_model_string_rep + ".h5")
    model_json = model.to_json()
    with open(base_model_string_rep + ".json", "w") as f:
        f.write(model_json)
    

    keras.backend.clear_session()

    return (test_loss, test_acc)
"""
        layers = 2
        dense_layers = 4
        nodes = [12, 12, 21, 34, 56, 105, 121]
        dense_nodes = [2, 2, 12, 12, 12, 68, 12]
        width_kernel = [2, 2, 2, 2, 1, 2, 3]
        height_kernel = [2, 2, 2, 1, 3, 2, 1]
        dropouts = [0, 0, 0, 0, 0, 0, 11]
        actual_net_code = build_network.generate_network_code(layers=layers, dense_layers=dense_layers, dense_nodes=dense_nodes, nodes=nodes, height_kernel=height_kernel, width_kernel=width_kernel, dropouts=dropouts)

        self.assertEqual(expected_net_code, actual_net_code)
