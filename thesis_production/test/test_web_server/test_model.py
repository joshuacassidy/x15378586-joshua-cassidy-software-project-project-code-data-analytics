import unittest, random
import web_server.models.model as model
import unittest.mock as mock

class Subprocess_Mock:

    def Popen(self, stdout):
        return None

class TestModel(unittest.TestCase):

    @mock.patch('subprocess.Popen', new=Subprocess_Mock.Popen)
    def test_generate_model_job(self):
        expected_results = [
            "python3 create_generator_model.py upload_directory file data_path http://localhost:8080 j@g.com None None None None None",
            "python3 create_generator_model.py upload_directory file data_path http://localhost:8080 j@g.com 0.8 0.5 True None None",
            "python3 create_generator_model.py upload_directory file data_path http://localhost:8080 j@g.com 0.8 0.5 True 4 30"
        ]
        input_data = [
            {
                'file_name':'file',
                'unstructured_data_path':'data_path',
                'email':'j@g.com',
                'base_url':'http://localhost:8080',
                'mutation_rate': None, 
                'crossover_rate': None, 
                'early_stopping': None, 
                'generations': None, 
                'population_size': None,
            },
            {
                'file_name':'file',
                'unstructured_data_path':'data_path',
                'email':'j@g.com',
                'base_url':'http://localhost:8080',
                'mutation_rate': 0.8, 
                'crossover_rate': 0.5, 
                'early_stopping': True, 
                'generations': None, 
                'population_size': None,
            },{
                'file_name':'file',
                'unstructured_data_path':'data_path',
                'email':'j@g.com',
                'base_url':'http://localhost:8080',
                'mutation_rate': 0.8, 
                'crossover_rate': 0.5, 
                'early_stopping': True, 
                'generations': 4, 
                'population_size': 30,
            },
        ]
        for i in range(len(input_data)):
            actual_output = model.generate_model_job(
                file_name=input_data[i]['file_name'],
                unstructured_data_path=input_data[i]['unstructured_data_path'],
                email=input_data[i]['email'],
                base_url=input_data[i]['base_url'],
                mutation_rate=input_data[i]['mutation_rate'],
                crossover_rate=input_data[i]['crossover_rate'],
                early_stopping=input_data[i]['early_stopping'],
                generations=input_data[i]['generations'],
                population_size=input_data[i]['population_size']
            )
            
            self.assertEqual(actual_output, expected_results[i])

    @mock.patch('subprocess.Popen', new=Subprocess_Mock.Popen)
    def test_predict_model_job(self):
        expected_result = "python3 create_predictor_model.py upload_directory file data_path http://localhost:8080 j@g.com"
        
        input_data = {
                'file_name':'file',
                'unstructured_data_path':'data_path',
                'email':'j@g.com',
                'base_url':'http://localhost:8080',
            }
            
        actual_output = model.predict_model_job(
            file_name=input_data['file_name'],
            unstructured_data_path=input_data['unstructured_data_path'],
            email=input_data['email'],
            base_url=input_data['base_url'],
        )
        self.assertEqual(actual_output, expected_result)
