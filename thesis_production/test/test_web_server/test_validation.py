import unittest, random
import web_server.models.vaildation as vaildation
import unittest.mock as mock


class TestModel(unittest.TestCase):

    def test_validate_email(self):
        request = [
            {},
            {'email': None},
            {'email': ''},
            {'email': '   '},
            {'email': '89'},
            {'email': 'josh'},
            {'email': 'josh.com'},
            {'email': 'josh@gcom'},
            {'email': 'josh@.com'},
            {'email': 'josh@gmail.com'},
            
        ]

        expected_results = [
            "Please enter a email",
            "Please enter a email",
            "Please enter a email",
            "Please enter a email",
            "Please enter a valid email",
            "Please enter a valid email",
            "Please enter a valid email",
            "Please enter a valid email",
            "Please enter a valid email",
            None
        ]


        for i in range(len(request)):
            actual_output = vaildation.validate_email(request[i])
            self.assertEqual(actual_output, expected_results[i])

    def test_validate_mutation_rate(self):
        request = [
            {},
            {'mutation_rate': None},
            {'mutation_rate': "ssad"},
            {'mutation_rate': 2},
            {'mutation_rate': -2},
            {'mutation_rate': 0.2},
        ]

        expected_results = [
            None,
            "The Mutation Rate needs to be between 1 and 0 for the genetic algorithms process",
            "The Mutation Rate needs to be between 1 and 0 for the genetic algorithms process",
            "The Mutation Rate needs to be between 1 and 0 for the genetic algorithms process",
            "The Mutation Rate needs to be between 1 and 0 for the genetic algorithms process",
            None
        ]

        for i in range(len(request)):
            actual_output = vaildation.validate_mutation_rate(request[i])
            self.assertEqual(actual_output, expected_results[i])

    def test_validate_crossover_rate(self):
        request = [
            {},
            {'crossover_rate': None},
            {'crossover_rate': "ssad"},
            {'crossover_rate': 2},
            {'crossover_rate': -2},
            {'crossover_rate': 0.2},
        ]

        expected_results = [
            None,
            "The Crossover Rate needs to be between 1 and 0 for the genetic algorithms process",
            "The Crossover Rate needs to be between 1 and 0 for the genetic algorithms process",
            "The Crossover Rate needs to be between 1 and 0 for the genetic algorithms process",
            "The Crossover Rate needs to be between 1 and 0 for the genetic algorithms process",
            None
        ]

        for i in range(len(request)):
            actual_output = vaildation.validate_crossover_rate(request[i])
            self.assertEqual(actual_output, expected_results[i])

    def test_validate_generations(self):
        request = [
            {},
            {'generations': None},
            {'generations': "ssad"},
            {'generations': 0},
            {'generations': -2},
            {'generations': 10},
        ]

        expected_results = [
            None,
            "The genetic algorithms process must run for atleast 1 generation",
            "The genetic algorithms process must run for atleast 1 generation",
            "The genetic algorithms process must run for atleast 1 generation",
            "The genetic algorithms process must run for atleast 1 generation",
            None
        ]

        for i in range(len(request)):
            actual_output = vaildation.validate_generations(request[i])
            self.assertEqual(actual_output, expected_results[i])

    def test_validate_population_size(self):
        request = [
            {},
            {'population_size': None},
            {'population_size': "ssad"},
            {'population_size': 2},
            {'population_size': -2},
            {'population_size': 10},
        ]

        expected_results = [
            None,
            "The genetic algorithms process must have a population size of atleast 3",
            "The genetic algorithms process must have a population size of atleast 3",
            "The genetic algorithms process must have a population size of atleast 3",
            "The genetic algorithms process must have a population size of atleast 3",
            None
        ]

        for i in range(len(request)):
            actual_output = vaildation.validate_population_size(request[i])
            self.assertEqual(actual_output, expected_results[i])

