import sys
import create_model

input_file, upload_dir, file_name, unstructured_data_path, base_url, email = sys.argv
create_model.create_predictor_model(
    upload_dir,
    file_name,
    unstructured_data_path,
    base_url,
    email
)