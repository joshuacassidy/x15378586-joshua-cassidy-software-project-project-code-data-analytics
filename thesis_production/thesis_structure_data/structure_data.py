import os, random, shutil
from distutils.dir_util import copy_tree

def populate_directory(dataset_source_directory, output_dir, directory_type, start_image_amount, images_amount, labels):
    for label in labels:
        if label.startswith("."):
            continue
        images = os.listdir(os.path.join(dataset_source_directory, label))
        random.shuffle(images)
        os.mkdir(os.path.join(output_dir, directory_type, label))
        if images_amount is None:
            end_images_amount = len(images)
        else:
            end_images_amount = images_amount
        for j in range(start_image_amount, end_images_amount):
            source_location = os.path.join(dataset_source_directory, label, images[j])
            destination_location = os.path.join(output_dir, directory_type, label, images[j])
            shutil.move(source_location, destination_location)

def copy_directory_contents(dataset_source_directory, output_dir, labels):
    for label in labels:
        if label.startswith("."):
            continue
        os.mkdir(os.path.join(output_dir, label))
    copy_tree(dataset_source_directory, output_dir)


def structure_data_helper(unstructured_data_path, labels, upload_dir, file_name):
    labels = [i for i in os.listdir(unstructured_data_path) if os.path.isdir(os.path.join(unstructured_data_path,i))]
    
    if len(labels) >= 2:
        output_dir = os.path.join(upload_dir, str(file_name + "_structured"))
        if not os.path.isdir(output_dir):

            os.mkdir(output_dir)

            test_directory = "test"
            train_directory = "train"
            validation_directory = "validation"

            train_directories = [train_directory, test_directory, validation_directory]
            for i in train_directories:
                os.mkdir(os.path.join(output_dir, i))

            test_amount = round(len(os.listdir(os.path.join(unstructured_data_path, labels[0]))) * 0.25)
            populate_directory(
                dataset_source_directory=unstructured_data_path, 
                output_dir=output_dir, 
                directory_type=test_directory, 
                start_image_amount=0,
                images_amount=test_amount,
                labels=labels
            )
            populate_directory(
                dataset_source_directory=unstructured_data_path, 
                output_dir=output_dir, 
                directory_type=train_directory, 
                start_image_amount=0,
                images_amount=None,
                labels=labels
            )
            copy_directory_contents(os.path.join(output_dir,train_directory), os.path.join(output_dir,validation_directory), labels)
    else:
        raise Exception("Dataset only contains one folder")



def structure_data(unstructured_data_path, upload_dir, file_name):
    labels = [i for i in os.listdir(unstructured_data_path) if os.path.isdir(os.path.join(unstructured_data_path,i))]
    if len(labels) == 1 or ("__MACOSX" in labels and len(labels) == 2):
        if "__MACOSX" in labels:
            labels.remove("__MACOSX")
        search_directory = os.path.join(unstructured_data_path, labels[0])
        labels = [i for i in os.listdir(search_directory) if os.path.isdir(os.path.join(search_directory,i))]
        structure_data_helper(search_directory, labels, upload_dir, file_name)
    elif len(labels) < 1:
        raise Exception("Dataset only contains one folder")
    else:
        structure_data_helper(unstructured_data_path, labels, upload_dir, file_name)
