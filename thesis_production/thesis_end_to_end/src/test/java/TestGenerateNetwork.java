import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;

public class TestGenerateNetwork {

    private WebDriver[] drivers;

    @Before
    public void setUp() throws Exception {
        this.drivers = new WebDriver[]{new FirefoxDriver(), new ChromeDriver()};
    }

    @Test
    public void generateNetwork() {

        for(WebDriver driver: drivers) {
            try {
                Thread.sleep(1000);
             } catch (Exception e) {
 
             }
            driver.get(Constants.URL);
            WebElement element = driver.findElement(By.id("zip"));
            element.sendKeys(new File(System.getProperty("user.dir"), "zip.zip").getPath());

            element = driver.findElement(By.id("email"));
            element.sendKeys("Joshcassidy79@gmail.com");

            element = driver.findElement(By.xpath("//button[contains(@type, 'submit')]"));
            element.click();

            Assert.assertEquals("Thank You!", driver.findElement(By.id("header")).getText());
            driver.close();
        }

    }

    @Test
    public void generateNetworkCustom() {

        for(WebDriver driver: drivers) {
            try {
                Thread.sleep(1000);
             } catch (Exception e) {
 
             }
            driver.get(Constants.URL + "/generate_custom");
            WebElement element = driver.findElement(By.id("zip"));
            element.sendKeys(new File(System.getProperty("user.dir"), "zip.zip").getPath());


            element = driver.findElement(By.id("email"));
            element.sendKeys("Joshcassidy79@gmail.com");

            element = driver.findElement(By.id("mutation_rate"));
            element.sendKeys("0.001");

            element = driver.findElement(By.id("crossover_rate"));
            element.sendKeys("0.5");

            element = driver.findElement(By.id("generations"));
            element.sendKeys("2");

            element = driver.findElement(By.id("population_size"));
            element.sendKeys("3");

            driver.findElement(By.xpath("//label[contains(@for, 'early_stopping')]")).click();

            element = driver.findElement(By.xpath("//button[contains(@type, 'submit')]"));
            element.click();

            Assert.assertEquals("Thank You!", driver.findElement(By.id("header")).getText());
            driver.close();
        }

    }


}
