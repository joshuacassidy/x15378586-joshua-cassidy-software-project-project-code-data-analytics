import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.HashMap;
import java.util.Map;

public class TestDownloadArchitecture {

    private WebDriver[] drivers;
    private String downloadDirectory;

    public TestDownloadArchitecture() {
        this.downloadDirectory = System.getProperty("user.dir");
    }

    @Before
    public void setUp() throws Exception {
        this.drivers = new WebDriver[]{new FirefoxDriver(), new ChromeDriver()};
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.dir", downloadDirectory);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/json, application/h5");


        Map<String, Object> chromePreferences = new HashMap<String, Object>();
        chromePreferences.put("download.default_directory", downloadDirectory);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePreferences);

    }

    @Test
    public void testDownloadNotFound() {
        for (WebDriver driver : drivers) {
            try {
                Thread.sleep(1000);
             } catch (Exception e) {
 
             }
            driver.get(Constants.URL + "/generated/1");
            Assert.assertEquals("Network Architecture Not Found", driver.findElement(By.id("header")).getText());
        }

        for (WebDriver driver : drivers) {
            try {
                Thread.sleep(1000);
             } catch (Exception e) {
 
             }
            driver.get(Constants.URL + "/predicted/1");
            Assert.assertEquals("Network Architecture Not Found", driver.findElement(By.id("header")).getText());
        }

        for (WebDriver driver : drivers) {
            driver.close();
        }


    }

    @Test
    public void testDownloadArchitecture() {
        for (WebDriver driver : drivers) {
            try {
                Thread.sleep(1000);
             } catch (Exception e) {
 
             }
            driver.get(Constants.URL + "/generated/4d260e67032a85dea81d1bc7ed24256ddc90d03d");

            Assert.assertEquals("Download Architecture", driver.findElement(By.id("header")).getText());
        }

        for (WebDriver driver : drivers) {
            try {
                Thread.sleep(1000);
             } catch (Exception e) {
 
             }
            driver.get(Constants.URL + "/predicted/4d260e67032a85dea81d1bc7ed24256ddc90d03d");

            Assert.assertEquals("Download Architecture", driver.findElement(By.id("header")).getText());
        }
        for (WebDriver driver : drivers) {
            driver.close();
        }

    }

}
