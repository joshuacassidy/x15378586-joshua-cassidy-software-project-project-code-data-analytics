import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestInstructions {

    WebDriver[] drivers;

    @Before
    public void setUp() throws Exception {
        this.drivers = new WebDriver[]{new FirefoxDriver(), new ChromeDriver()};
    }

    @Test
    public void testInstructions() {
        for (WebDriver driver : drivers) {
            try {
               Thread.sleep(1000);
            } catch (Exception e) {

            }
            driver.get(Constants.URL + "/instructions");
            Assert.assertEquals("Dataset Guidelines for the CNNHGAO", driver.findElement(By.id("header")).getText());
            driver.close();
        }

    }

}



