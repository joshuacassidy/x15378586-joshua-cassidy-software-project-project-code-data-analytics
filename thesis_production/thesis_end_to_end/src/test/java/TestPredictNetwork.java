import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;

public class TestPredictNetwork {

    private WebDriver[] drivers;

    @Before
    public void setUp() throws Exception {
        this.drivers = new WebDriver[]{new FirefoxDriver(), new ChromeDriver()};
    }

    @Test
    public void predictNetwork() {
        for(WebDriver driver: drivers) {
            try {
                Thread.sleep(1000);
             } catch (Exception e) {
 
             }
            driver.get(Constants.URL + "/predict");
            WebElement element = driver.findElement(By.id("zip"));
            element.sendKeys(new File(System.getProperty("user.dir"), "zip.zip").getPath());

            element = driver.findElement(By.id("email"));
            element.sendKeys("Joshcassidy79@gmail.com");

            element = driver.findElement(By.xpath("//button[contains(@type, 'submit')]"));
            element.click();

            Assert.assertEquals("Thank You!", driver.findElement(By.id("header")).getText());
            driver.close();
        }


    }
}
