from keras.models import load_model
import os, numpy as np, json, keras

def generate_network(compiled_signal_data):
    models_locations = os.listdir('models')

    for i in range(len(models_locations)-1, -1, -1):
        if models_locations[i].startswith('_') or models_locations[i].startswith('.'):
            models_locations.remove(models_locations[i])


    models = []



    models_locations = sorted(models_locations, key=lambda x: int(x.split('net_')[-1].split('_')[0]))
    model_str_rep = ''
    for i in models_locations:
        if not i.startswith('.'):
            model_str_rep += str(load_model('models/' + i).predict_classes(np.array([compiled_signal_data]))[0][0])
            keras.backend.clear_session()
    return model_str_rep