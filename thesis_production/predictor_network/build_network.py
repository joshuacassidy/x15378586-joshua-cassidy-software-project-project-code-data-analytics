from predictor_network.net_template import code
import os, math, json, datetime, pymongo
import thesis_model_serialisation.network_representation as network_representation




def generate_network_code(layers, dense_layers, nodes, dense_nodes, width_kernel, height_kernel, dropouts):
    
    for i in range(len(dropouts)):
        if dropouts[i] > 100:
            dropouts[i] = 100
            

    layers_str = ""
    dense_layers_str = ""
    layers_template = """model.add(Conv2D({nodes}, ({kernel_width}, {kernel_height}), activation="relu"))"""
    dropout_template = "model.add(Dropout({weight}))\n    "
    dense_layer_template = """model.add(Dense({nodes}, activation="relu"))\n    """

    for i in range(0, layers):
        if i != 0:
            layers_str += "    "
        nodes_current = nodes[-(i+1)]
        if nodes_current == 0:
            nodes_current = 1
        width_kernel_current = width_kernel[-(i+1)]
        if width_kernel_current == 0:
            width_kernel_current = 1
        height_kernel_current = height_kernel[-(i+1)]
        if height_kernel_current == 0:
            height_kernel_current = 1
        if dropouts[-(i+1)] != 0:
                layers_str += dropout_template.format(weight=dropouts[-(i+1)]/100)
        layers_str += layers_template.format(nodes=nodes_current, kernel_width=width_kernel_current, kernel_height=height_kernel_current) + "\n"
    for i in range(dense_layers):
        nodes_current = dense_nodes[-(i+1)]
        if nodes_current == 0:
            nodes_current = 1
        dense_layers_str += dense_layer_template.format(nodes=nodes_current)

    network_code = code.format(layers=layers_str, dense_layers=dense_layers_str)
    return network_code


def build_network(byte_str, base_dir, output_directory):
    start = datetime.datetime.now()

    network_components = network_representation.deserialise(byte_str)
    layers = network_components["layers"]
    dense_layers = network_components["dense_layers"]
    nodes = network_components["nodes"]
    dense_nodes = network_components["dense_nodes"]
    width_kernel = network_components["width_kernel"]
    height_kernel = network_components["height_kernel"]
    dropouts = network_components["dropouts"]

    network_code = generate_network_code(
        layers=layers, 
        dense_layers=dense_layers, 
        nodes=nodes, 
        dense_nodes=dense_nodes, 
        width_kernel=width_kernel,
        height_kernel=height_kernel,
        dropouts=dropouts
    )


    train_dir = os.path.join(base_dir, "train")

    validation_dir = os.path.join(base_dir, "validation")

    test_dir = os.path.join(base_dir, "test")

    

    execute_net_code = """create_neural_net(
        train_dir="{train_dir}", 
        validation_dir="{validation_dir}", 
        test_dir="{test_dir}",
        output_directory="{output_directory}"
    )""".format(
        train_dir=train_dir, 
        validation_dir=validation_dir,
        test_dir=test_dir,
        output_directory = output_directory
    )
    context = {}
    exec(network_code + "\ntest_loss, test_acc = " + execute_net_code, context)
    end = datetime.datetime.now()

    loss = context["test_loss"]
    acc = context["test_acc"]
    compile_time = end - start


    image_set_hash = base_dir.split("/")[-1]
    image_set_hash = image_set_hash.split("_structured")[0]

    model_file_name = output_directory + "/" + str(acc) + ":" + str(loss)

    with open(model_file_name + ".json", "r") as f:
        json_data = json.load(f)

    classes = [i for i in os.listdir(train_dir) if not i.startswith(".") or i.startswith("_") ]




    record = { 
        "nodes": nodes,
        "dense_nodes": dense_nodes,
        "dense_layers": dense_layers,
        "dropouts": dropouts,
        "dataset": base_dir,
        "kernel_width": width_kernel, 
        "kernel_height": height_kernel,
        "labels": classes,
        "acc": acc,
        "loss": loss,
        "compile_time": int(compile_time.seconds),
        "image_set_hash": image_set_hash,
        "model": model_file_name + ".h5",
        "arch": json_data,
        "model_vis": model_file_name + ".png"
    }


    loss = context["test_loss"]
    acc = context["test_acc"]


    client = pymongo.MongoClient("mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/?compressors=disabled&gssapiServiceName=mongodb",authSource="thesis")

    best_networks = client["thesis"]["predicted_networks"]
    best_networks.insert_one(record)
    return (loss, acc)
