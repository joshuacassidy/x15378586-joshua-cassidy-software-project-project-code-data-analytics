import random
from . import genes
from .population import Population
from .member import Member
from .utils import net_generator, genetics_utils, genes_utils, mutate_utils, image_utils

class Genetics:

    def __init__(self, base_dir=None):
        self.base_dir = base_dir

    def evolve_population(self, population, base_dir, classes, early_stopping=False):
        self.base_dir = base_dir
        next_generation = Population(population.size, generate_new_members=False, base_dir=self.base_dir, classes=classes)
        for i in range(population.size):
            parent1 = self.get_fittest(population)
            parent2 = self.get_fittest(population)
            child = self.crossover(parent1, parent2, classes, early_stopping)
            next_generation.members[i] = child
        for i in range(population.size):
            self.mutate(next_generation.members[i])

        return next_generation

    def crossover(self, parent1, parent2, classes, early_stopping=False):
        child = Member(new_member=False, base_dir=self.base_dir, classes=classes, early_stopping=early_stopping)
        genetics_utils.generate_child_layers(
            child_layers=child.layers, 
            parent1_layers=parent1.layers, 
            parent2_layers=parent2.layers
        )
        mutate_utils.mutate_layers(child_layers=child.layers)
        
        genetics_utils.generate_child_layers(
            child_layers=child.dense_layers, 
            parent1_layers=parent1.dense_layers, 
            parent2_layers=parent2.dense_layers
        )
        mutate_utils.mutate_layers(child_layers=child.dense_layers)


        layers_amount = genes_utils.genes_to_int(child.layers)
        dense_layers_amount = genes_utils.genes_to_int(child.dense_layers)

        genetics_utils.generate_child_nodes(
            child_nodes=child.nodes, 
            child_layers=child.layers, 
            parent1_nodes=parent1.nodes, 
            parent2_nodes=parent2.nodes, 
            layers_amount=layers_amount
        )

        genetics_utils.generate_child_nodes(
            child_nodes=child.dense_nodes, 
            child_layers=child.dense_layers, 
            parent1_nodes=parent1.dense_nodes, 
            parent2_nodes=parent2.dense_nodes, 
            layers_amount=dense_layers_amount
        )

        genetics_utils.generate_child_dropouts(child, parent1, parent2, layers_amount)
        downsized_image_width, downsized_image_height = image_utils.dataset_dimensions(self.base_dir)

        child.kernel_width = genetics_utils.generate_child_kernel(
            child=child, 
            parent1_kernel=parent1.kernel_width,
            parent2_kernel=parent2.kernel_width, 
            layers_amount=layers_amount,
            downsized_image_size=downsized_image_width)

        child.kernel_height = genetics_utils.generate_child_kernel(
            child=child, 
            parent1_kernel=parent1.kernel_height,
            parent2_kernel=parent2.kernel_height, 
            layers_amount=layers_amount,
            downsized_image_size=downsized_image_height)
        return child


    def mutate(self, member):
        mutate_utils.mutate_nodes(child_nodes=member.nodes, child_layers=member.layers)
        mutate_utils.mutate_nodes(child_nodes=member.dense_nodes, child_layers=member.dense_layers)

        mutate_utils.mutate_dropouts(member)

        downsized_image_width, downsized_image_height = image_utils.dataset_dimensions(self.base_dir)
        mutate_utils.mutate_kernel(member.kernel_width, downsized_image_width)
        mutate_utils.mutate_kernel(member.kernel_height, downsized_image_height)

        
    def get_fittest(self, population):
        tournament_size = 7 if int(population.size/2) > 7 else int(population.size/2)
        fittest = random.choice(population.members)

        for _ in range(tournament_size):
            current = random.choice(population.members)
            if current.fitness > fittest.fitness:
                fittest = current
        return fittest

    def __str__(self):
        return 'Population()'

