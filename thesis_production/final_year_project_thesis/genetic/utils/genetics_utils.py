from .. import genes as genes
import random
from . import genes_utils, genetics_utils, net_generator

def generate_child_layers(child_layers, parent1_layers, parent2_layers):
    for i in range(genes.LAYERS):
        if random.uniform(0, 1) < genes.CROSSOVER_RATE:
            child_layers.append(parent1_layers[i])
        else:
            child_layers.append(parent2_layers[i])
    net_generator.validate_layers(layers=child_layers)

def generate_child_nodes(child_layers, child_nodes, parent1_nodes, parent2_nodes, layers_amount):
    for layer in range(layers_amount):
        temp_nodes = []
        parent1_nodes_temp = parent1_nodes[layer % len(parent1_nodes)]
        parent2_nodes_temp = parent2_nodes[layer % len(parent2_nodes)]
        for i in range(genes.NODES):
            if random.uniform(0, 1) < genes.CROSSOVER_RATE:
                temp_nodes.append(parent1_nodes_temp[i % len(parent1_nodes_temp)])
            else:
                temp_nodes.append(parent2_nodes_temp[i % len(parent2_nodes_temp)])
        child_nodes.append(temp_nodes)
    child_nodes = net_generator.validate_nodes(layers=child_layers, nodes=child_nodes)


def generate_child_kernel(child, parent1_kernel, parent2_kernel, layers_amount, downsized_image_size):
    child_kernel = []
    for kernel in range(layers_amount):
        temp_kernel = []
        for i in range(genes.WIDTH_KERNEL):
            if random.uniform(0, 1) < genes.CROSSOVER_RATE:
                parent_kernel = parent1_kernel[kernel % len(parent1_kernel)]
                temp_kernel.append(parent_kernel[i])
            else:
                parent_kernel = parent2_kernel[kernel % len(parent2_kernel)]
                temp_kernel.append(parent_kernel[i])
        net_generator.validate_kernel(temp_kernel)
        kernel_size = genes_utils.genes_to_int(temp_kernel)

        if downsized_image_size - (kernel_size-1) >= 1:
            downsized_image_size = downsized_image_size - (kernel_size-1)

        if downsized_image_size - (kernel_size-1) <= 1:
            random_kernel = random.randint(1, max([downsized_image_size, 1]))
            downsized_image_size = downsized_image_size - (random_kernel-1)
            temp_kernel = genes_utils.int_to_genes(random_kernel, genes.WIDTH_KERNEL)
        child_kernel.append(temp_kernel)
    return child_kernel

def generate_child_dropouts(child, parent1, parent2, layers_amount):
    for layer in range(layers_amount):
        
        parent1_dropouts = parent1.dropouts[layer % len(parent1.dropouts)]
        parent2_dropouts = parent2.dropouts[layer % len(parent2.dropouts)]
        temp_dropout = []
        for dropout_gene in range(7):
            if random.uniform(0, 1) < genes.CROSSOVER_RATE:
                temp_dropout.append(parent1_dropouts[dropout_gene])
            else:
                temp_dropout.append(parent2_dropouts[dropout_gene])
        if genes_utils.genes_to_int(temp_dropout) > 100:
            temp_dropout = genes_utils.int_to_genes(100, len(genes.DEFAULT_DROPOUT_LAYER))
        child.dropouts.append(temp_dropout)


