def genes_to_int(genes):
    genes_string = '0b' + (''.join(str(i) for i in genes))
    genes_int = int(genes_string, 2)
    return genes_int


def int_to_genes(integer, genes_size):
    binary_number = bin(integer).replace('0b', '')
    number_padding = ''.join(['0' for i in range(genes_size-len(binary_number))])
    return [int(i) for i in list(number_padding+binary_number)]
