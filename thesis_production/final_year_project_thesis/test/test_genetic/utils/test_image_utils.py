import unittest
import unittest.mock as mock
from random import choice
import random, os
from genetic import genes
from genetic.utils import image_utils
from genetic.member import Member
from genetic.genetics import Genetics
import matplotlib.image
import numpy as np

class TestGenetics(unittest.TestCase):

    @mock.patch('genetic.utils.image_utils.list_files', return_value=['/'])
    @mock.patch('matplotlib.image.imread', return_value=np.array([[0., 0.], [0., 0.]]))
    def test_dataset_dimensions(self, list_files_mock, image_dimensions_mock):
        self.assertEqual(image_utils.dataset_dimensions("/"), (2,2))
    
    @mock.patch('os.listdir', return_value=['x', 'y', 'z', '.hidden'])
    def test_list_files(self, os_listdir_mock):
        self.assertEqual(image_utils.list_files('/'), ['x', 'y', 'z'])