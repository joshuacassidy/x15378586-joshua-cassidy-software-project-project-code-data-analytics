import unittest, random
import genetic.utils.genetics_utils as genetics_utils
from genetic.member import Member
import genetic.utils.genes_utils as genes_utils
from genetic import genes
import unittest.mock as mock




class TestGenetics(unittest.TestCase):

    @mock.patch('genetic.genes.LAYERS', 3)
    @mock.patch('genetic.genes.NODES', 8)
    def test_generate_network_code(self):
        net_code = """
import datetime
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os, shutil, math
import pymongo

from keras import layers
from keras import models
from keras.utils import to_categorical
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import keras
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.image as mpimg
from keras.utils import plot_model


def list_files(path):
    return [f for f in os.listdir(path) if not f.startswith('.')]

def create_neural_net(base_dir, train_dir, validation_dir, test_dir, classes_categories, patience = 2, early_stopping='False'):
    train_datagen = ImageDataGenerator(
            rescale=1./255,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True
        )
    
    validation_datagen = ImageDataGenerator(rescale=1./255)

    classes = len(list_files(train_dir)) if classes_categories == 'null' else len(classes_categories.split(","))
    classification_folder_name = list_files(train_dir)[0]
    images_path = train_dir + "/" + classification_folder_name
    image_name = list_files(images_path)[0]
    image_path = train_dir + "/" + classification_folder_name + "/" + image_name

    image_shape = mpimg.imread(image_path).shape
    model = Sequential()
    model.add(Conv2D(64, kernel_size=(3, 3), 
                    activation='relu',
                    input_shape=(image_shape[0], image_shape[1], 3)))
    model.add(Dropout(1))
    model.add(Conv2D(193, (1, 3), activation='relu'))
    model.add(Dropout(0.15))
    model.add(Conv2D(221, (3, 2), activation='relu'))

    model.add(Flatten())
    model.add(Dense(124, activation='relu'))
    model.add(Dense(81, activation='relu'))
    model.add(Dense(207, activation='relu'))
    model.add(Dense(54, activation='relu'))
    
    model.add(Dense(classes, activation='softmax'))

    model.compile(
        loss=keras.losses.categorical_crossentropy,
        optimizer=keras.optimizers.Adadelta(),
        metrics=['accuracy']
    )
    test_datagen = ImageDataGenerator(rescale=1./255)
    if classes_categories == 'null':
        train_generator = train_datagen.flow_from_directory(
            train_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical'
        )

        validation_generator = train_datagen.flow_from_directory(
            validation_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical'
        )

        test_generator = test_datagen.flow_from_directory(
            test_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical'
        )
    else:
        train_generator = train_datagen.flow_from_directory(
            train_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical',
            classes = classes_categories.split(",")            
        )

        validation_generator = train_datagen.flow_from_directory(
            validation_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical',
            classes = classes_categories.split(",")
        )

        test_generator = test_datagen.flow_from_directory(
            test_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical',
            classes = classes_categories.split(",")
        )

    if early_stopping == 'True':
        history = model.fit_generator(
            train_generator, 
            steps_per_epoch=100, 
            epochs=40, 
            validation_data=validation_generator,
            validation_steps=10,
            workers=12,
            max_queue_size=20, 
            callbacks=[keras.callbacks.EarlyStopping(monitor='accuracy', mode='max', patience=2, restore_best_weights=True)]

        )
    else:
        history = model.fit_generator(
            train_generator, 
            steps_per_epoch=100, 
            epochs=40, 
            validation_data=validation_generator,
            validation_steps=10,
            workers=12,
            max_queue_size=20
        )

    
    test_loss, test_acc = model.evaluate_generator(
        test_generator, 
        steps=10,
        workers=5
    )

    base_model_string_rep = base_dir + "_models/" + str(math.exp(math.exp(math.exp(test_acc - test_loss)))) + ":" + str(test_acc) + ":" + str(test_loss)

    model.save(base_model_string_rep + ".h5")
    model_json = model.to_json()
    with open(base_model_string_rep + ".json", "w") as f:
        f.write(model_json)
    

    keras.backend.clear_session()
    return (test_loss, test_acc)

    
    
"""

        member = Member(new_member=False, base_dir="/", classes=None)
        member.layers=[0, 1, 0] 
        member.nodes=[
            [1, 1, 0, 0, 0, 0, 0, 1],
            [1, 1, 0, 1, 1, 1, 0, 1]
        ]
        member.kernel_width=[[0, 1], [1, 1]]
        member.kernel_height=[[1, 1], [1, 0]]
        member.dropouts=[
            [1, 1, 1, 1, 1, 1, 1],
            [0, 0, 0, 1, 1, 1, 1]
            ]
        member.dense_layers=[1, 0, 0]
        member.dense_nodes=[[0, 1, 1, 1, 1, 1, 0, 0], [0, 1, 0, 1, 0, 0, 0, 1], [1, 1, 0, 0, 1, 1, 1, 1], [0, 0, 1, 1, 0, 1, 1, 0]]
        self.assertEqual(net_code, member.generate_network_code())
    
    @mock.patch('genetic.genes.LAYERS', 3)
    @mock.patch('genetic.genes.NODES', 8)
    def test_generate_network_code_no_dropouts(self):
        net_code = """
import datetime
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
import os, shutil, math
import pymongo

from keras import layers
from keras import models
from keras.utils import to_categorical
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import keras
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.image as mpimg
from keras.utils import plot_model


def list_files(path):
    return [f for f in os.listdir(path) if not f.startswith('.')]

def create_neural_net(base_dir, train_dir, validation_dir, test_dir, classes_categories, patience = 2, early_stopping='False'):
    train_datagen = ImageDataGenerator(
            rescale=1./255,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True
        )
    
    validation_datagen = ImageDataGenerator(rescale=1./255)

    classes = len(list_files(train_dir)) if classes_categories == 'null' else len(classes_categories.split(","))
    classification_folder_name = list_files(train_dir)[0]
    images_path = train_dir + "/" + classification_folder_name
    image_name = list_files(images_path)[0]
    image_path = train_dir + "/" + classification_folder_name + "/" + image_name

    image_shape = mpimg.imread(image_path).shape
    model = Sequential()
    model.add(Conv2D(64, kernel_size=(3, 3), 
                    activation='relu',
                    input_shape=(image_shape[0], image_shape[1], 3)))
    model.add(Conv2D(255, (3, 3), activation='relu'))
    model.add(Conv2D(191, (2, 2), activation='relu'))
    model.add(Conv2D(223, (1, 3), activation='relu'))
    model.add(Conv2D(239, (2, 3), activation='relu'))
    model.add(Conv2D(247, (3, 3), activation='relu'))
    model.add(Conv2D(251, (3, 3), activation='relu'))
    model.add(Conv2D(253, (3, 3), activation='relu'))

    model.add(Flatten())
    model.add(Dense(54, activation='relu'))
    model.add(Dense(124, activation='relu'))
    model.add(Dense(81, activation='relu'))
    model.add(Dense(81, activation='relu'))
    model.add(Dense(207, activation='relu'))
    model.add(Dense(54, activation='relu'))
    
    model.add(Dense(classes, activation='softmax'))

    model.compile(
        loss=keras.losses.categorical_crossentropy,
        optimizer=keras.optimizers.Adadelta(),
        metrics=['accuracy']
    )
    test_datagen = ImageDataGenerator(rescale=1./255)
    if classes_categories == 'null':
        train_generator = train_datagen.flow_from_directory(
            train_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical'
        )

        validation_generator = train_datagen.flow_from_directory(
            validation_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical'
        )

        test_generator = test_datagen.flow_from_directory(
            test_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical'
        )
    else:
        train_generator = train_datagen.flow_from_directory(
            train_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical',
            classes = classes_categories.split(",")            
        )

        validation_generator = train_datagen.flow_from_directory(
            validation_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical',
            classes = classes_categories.split(",")
        )

        test_generator = test_datagen.flow_from_directory(
            test_dir,
            target_size=(image_shape[0], image_shape[1]),
            class_mode='categorical',
            classes = classes_categories.split(",")
        )

    if early_stopping == 'True':
        history = model.fit_generator(
            train_generator, 
            steps_per_epoch=100, 
            epochs=40, 
            validation_data=validation_generator,
            validation_steps=10,
            workers=12,
            max_queue_size=20, 
            callbacks=[keras.callbacks.EarlyStopping(monitor='accuracy', mode='max', patience=2, restore_best_weights=True)]

        )
    else:
        history = model.fit_generator(
            train_generator, 
            steps_per_epoch=100, 
            epochs=40, 
            validation_data=validation_generator,
            validation_steps=10,
            workers=12,
            max_queue_size=20
        )

    
    test_loss, test_acc = model.evaluate_generator(
        test_generator, 
        steps=10,
        workers=5
    )

    base_model_string_rep = base_dir + "_models/" + str(math.exp(math.exp(math.exp(test_acc - test_loss)))) + ":" + str(test_acc) + ":" + str(test_loss)

    model.save(base_model_string_rep + ".h5")
    model_json = model.to_json()
    with open(base_model_string_rep + ".json", "w") as f:
        f.write(model_json)
    

    keras.backend.clear_session()
    return (test_loss, test_acc)

    
    
"""

        member = Member(new_member=False, base_dir="/", classes=None)
        member.layers=[1, 1, 1] 
        member.nodes=[
            [1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 1, 1, 1, 1, 1, 1],
            [1, 1, 0, 1, 1, 1, 1, 1],
            [1, 1, 1, 0, 1, 1, 1, 1],
            [1, 1, 1, 1, 0, 1, 1, 1],
            [1, 1, 1, 1, 1, 0, 1, 1],
            [1, 1, 1, 1, 1, 1, 0, 1],
        ]
        member.kernel_width=[
            [1, 1],
            [1, 0],
            [0, 1],
            [1, 0],
            [1, 1],
            [1, 1],
            [1, 1],
        ]
        member.kernel_height=[
            [1, 1],
            [1, 0],
            [1, 1],
            [1, 1],
            [1, 1],
            [1, 1],
            [1, 1],
        ]
        member.dropouts=[
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]
        member.dense_layers=[1, 1, 0]
        member.dense_nodes=[
            [0, 0, 1, 1, 0, 1, 1, 0],
            [0, 1, 1, 1, 1, 1, 0, 0], 
            [0, 1, 0, 1, 0, 0, 0, 1], 
            [0, 1, 0, 1, 0, 0, 0, 1], 
            [1, 1, 0, 0, 1, 1, 1, 1], 
            [0, 0, 1, 1, 0, 1, 1, 0]
        ]

        self.assertEqual(net_code, member.generate_network_code())
