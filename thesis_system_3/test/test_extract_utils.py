import unittest, random
import unittest.mock as mock
import extract_utils

class Data_Mock:

    def find(self, query):
        return [{"_id": "id"}]

class TestGenetics(unittest.TestCase):

    def test_get_collection_data(self):
        collection_data = extract_utils.get_collection_data('mock', 
            {
                'thesis': {
                    "mock": Data_Mock()
                }
            }
        )
        self.assertEqual([{"_id": "id"}], collection_data)

