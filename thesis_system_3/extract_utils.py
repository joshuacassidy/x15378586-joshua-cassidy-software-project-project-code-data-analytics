import pymongo, json

def get_collection_data(collection_name, client):
    networks = []
    for i in client['thesis'][collection_name].find({}):
        i["_id"] = str(i["_id"])
        networks.append(i)
    return networks

def extract(collection_name): 
    client = pymongo.MongoClient("mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/",authSource="thesis")
    networks = []
    with open(collection_name + '.json', "w") as f:
        networks = get_collection_data(collection_name, client)
        json.dump(networks, f)
    client.close()