This folder is organised with the model generator backend in the root of due to the Google Colab notebook code requiring it to be there.

The Documentation folder holds all of the project's documentation so far.

The Google-Colab-Notebook folder holds the backend prototype code.

Setup: 
1. Navigate to https://colab.research.google.com/notebooks/
2. Go to the upload tab
3. Upload the Final_Year_Project_Prototype.ipynb (Note ensure the notebook is using python3)

Execution
1. Click the run button on the first cell to extract cifar10 dataset
2. Click the run button on the second cell clone the backend code from GitHub into Colab
3. Click the run button on the third cell click the third cell to generate architectures by executing the backend code

The Prototype-Dataset-Sampling folder holds the script that takes a sample of the CIFAR10 dataset for the prototype
Prerequisites:
1. Have python3 installed
2. Have the Keras, PIL, and TensorFlow python modules installed
3. Have "python3" set as an environment variable for python3

Execution
1. change directory into the "Prototype-Dataset-Sampling" folder using the "cd" command
2. run the command "python3 cifar-10-jpg.py"

The Prototype-UI folder holds the GUI for the prototype for sub-systems 1 and 2
Prerequisites
1. Have Google Chrome installed

Execution
1. Open the desired page to view in Google Chrome

The Prototype-Web-Service folder holds the web service that allows the prototype to connect to the database
Setup
1. Have python3 installed
2. Have bson, flask, and pymongo modules installed
3. Have "python3" set as an environment variable for python3

Execution
1. change directory into the "Prototype-Web-Service" folder using the "cd" command
2. run the command "python3 app.py"

Note the web service is hosted at: https://ncirl-final-year-project.herokuapp.com

The test folder holds the prototypes test suit
Prerequisites:
1. Have python3 installed
2. Have the unittest, keras, PIL, and tensorflow python modules installed
3. Have "python3" set as an environment variable for python3

Execution
1. change directory into the prototypes root folder using the "cd" command
2. run the command "python3 -m unittest discover test"

The Prototype-Analytics-Engine holds dashboard containing reports and dashboards for the purposes of the prototype
Prequisites
1. Have Tableau Desktop 2019.4 installed

Execution
1. Open the "Analytics-Engine-Prototype.twb" file with Tableau Desktop in the Prototype-Analytics-Engine folder
2. Ensure the "Analytics-Engine-Prototype.twb" file is using the "networks.json" file as a datasource (located in the Prototype-Analytics-Engine aswell)