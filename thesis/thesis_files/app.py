from .genetic.genetics import Genetics
from .genetic.population import Population
from .genetic.member import Member
from .genetic.utils import genes_utils
import random
from .genetic import genes
from numpy.random import seed
import json
import requests, os, pymongo, math

def get_member_data(mem, base_dir, classes, generation):
    image_set_hash = base_dir.split("/")[-1]
    image_set_hash = image_set_hash.split("_structured")[0]

    model_file_name = base_dir + "_models/" + str(mem.fitness) + ":" + str(mem.acc) + ":" + str(mem.loss)

    with open(model_file_name + ".json", "r") as f:
            json_data = json.load(f)

    record = { 
        "generation": generation,
        "nodes": list([genes_utils.genes_to_int(i) for i in mem.nodes]),
        "layers": genes_utils.genes_to_int(mem.layers),
        "dense_nodes": list([genes_utils.genes_to_int(i) for i in mem.dense_nodes]),
        "dense_layers": genes_utils.genes_to_int(mem.dense_layers),
        "dropouts": list([genes_utils.genes_to_int(i) for i in mem.dropouts]),
        "dataset": mem.base_dir,
        "kernel_width": list([genes_utils.genes_to_int(i) for i in mem.kernel_width]), 
        "kernel_height": list([genes_utils.genes_to_int(i) for i in mem.kernel_height]),
        "fitness": mem.fitness,
        "labels": classes,
        "acc": mem.acc,
        "loss": mem.loss,
        "compile_time": int(mem.compile_time.seconds),
        "image_set_hash": image_set_hash,
        "model": model_file_name + ".h5",
        "arch": json_data,
        "model_vis": model_file_name + ".png"
    }
    return record
    

def generate_networks(base_dir, classes=None, early_stopping=False, population_size = 20, generations = 10, crossover_rate = None, mutation_rate = None):
    if crossover_rate is not None:
        genes.CROSSOVER_RATE = crossover_rate
    
    if mutation_rate is not None:
        genes.MUTATION_RATE = mutation_rate
    labels = os.listdir(os.path.join(base_dir, "train"))

    try:
        os.mkdir(base_dir + "_models/")
    except:
        pass

    try:
        labels.remove(".DS_Store")
    except:
        pass

    generation = 0
    entire_population = []

    __genetics = Genetics()
    __population = Population(population_size, generate_new_members=True, base_dir=base_dir, classes=classes, early_stopping=early_stopping)

    

    for mem in __population.members:
        
        record = get_member_data(mem=mem, base_dir=base_dir, classes=classes, generation=generation)
        entire_population.append(record)
    fittest = __population.get_fittest_member()

    for i in range(0, generations):
        generation = i + 1
        __population = __genetics.evolve_population(__population, base_dir=base_dir, classes=classes)
        for member in __population.members:
            member.evaluation()
            record = get_member_data(mem=mem, base_dir=base_dir, classes=classes, generation=generation)
            entire_population.append(record)
        if __population.get_fittest_member().fitness > fittest.fitness:
            fittest = __population.get_fittest_member()
        if early_stopping and generation >= 2 and fittest.acc > 0.7:
            break
        
    client = pymongo.MongoClient("mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/?compressors=disabled&gssapiServiceName=mongodb",authSource="thesis")

    all_networks = client['thesis']['all_generated_networks']
    
        
    for record in entire_population:
        if classes == None:
            data_classes = [i for i in os.listdir(os.path.join(base_dir, 'train')) if not i.startswith('.') or i.startswith('_') ]
            record['labels'] = data_classes
        all_networks.insert_one(record)

    record = get_member_data(mem=fittest, base_dir=base_dir, classes=classes, generation=generation)
    if classes == None:
            data_classes = [i for i in os.listdir(os.path.join(base_dir, 'train')) if not i.startswith('.') or i.startswith('_') ]
            record['labels'] = data_classes
    best_networks = client['thesis']['best_generated_networks']
    best_networks.insert_one(record)

    client.close()
    return (record['loss'], record['acc'])
    
