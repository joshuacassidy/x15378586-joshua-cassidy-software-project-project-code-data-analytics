from .. import genes as genes
import random
from . import genes_utils, net_generator

def mutate_kernel(child_kernel, downsized_image_size):
    for kernel in range(len(child_kernel)):
        for i in range(genes.WIDTH_KERNEL):
            if random.uniform(0, 1) < genes.MUTATION_RATE:
                    if child_kernel[kernel][i] == 1:
                        child_kernel[kernel][i] = 0
                    else:
                        child_kernel[kernel][i] = 1
        net_generator.validate_kernel(child_kernel[kernel])
        kernel_size = genes_utils.genes_to_int(child_kernel[kernel])

        if downsized_image_size - (kernel_size-1) >= 1:
            downsized_image_size = downsized_image_size - (kernel_size-1)

        if downsized_image_size - (kernel_size-1) <= 1:
            random_kernel = random.randint(1, max([downsized_image_size, 1]))
            downsized_image_size = downsized_image_size - (random_kernel-1)
            child_kernel[kernel] = genes_utils.int_to_genes(random_kernel, genes.WIDTH_KERNEL)

def mutate_layers(child_layers):
    for i in range(len(child_layers)):
        if random.uniform(0, 1) < genes.MUTATION_RATE:
            if child_layers[i] == 1:
                child_layers[i] = 0
            else:
                child_layers[i] = 1
    net_generator.validate_layers(layers=child_layers)

def mutate_nodes(child_nodes, child_layers):
    for nodes in child_nodes:
        for i in range(len(nodes)):
            if random.uniform(0, 1) < genes.MUTATION_RATE:
                if nodes[i] == 1:
                    nodes[i] = 0
                else:
                    nodes[i] = 1
    net_generator.validate_nodes(layers=child_layers, nodes=child_nodes)


def mutate_dropouts(child):
    for dropout in range(len(child.dropouts)):
        for dropout_index in range(len(child.dropouts[dropout])):
            if random.uniform(0, 1) < genes.MUTATION_RATE:
                if child.dropouts[dropout][dropout_index] == 1:
                    child.dropouts[dropout][dropout_index] = 0
                else:
                    child.dropouts[dropout][dropout_index] = 1
        if genes_utils.genes_to_int(child.dropouts[dropout]) > 100:
            child.dropouts[dropout] = genes_utils.int_to_genes(100, len(genes.DEFAULT_DROPOUT_LAYER))

        