import random
from .. import genes as genes
from . import genes_utils

def validate_layers(layers):
    is_null = True
    for layer in layers:
        if layer == 1:
            is_null = False
            break

    if is_null:
        layers[len(layers) - 1] = 1
    return layers

def validate_nodes(layers, nodes):
    mutated_layers = validate_layers(layers)

    for node in nodes:
        for i in node:
            is_null = True
            if i == 1:
                is_null = False
                break
        if is_null:
            node[len(node) - 1] = 1

    return nodes

def validate_kernel(kernel):
    is_null = True
    for k in kernel:
        if k == 1:
            is_null = False
            break

    if is_null:
        kernel[len(kernel) - 1] = 1
    return kernel

def generate_kernel(layers_amount, downsized_image_size):
    kernel = []
    for i in range(layers_amount):
        temp_kernel = []
        for _ in range(genes.WIDTH_KERNEL):
            temp_kernel.append(random.randint(0, 1))
        validate_kernel(temp_kernel)
        kernel_size = genes_utils.genes_to_int(temp_kernel)

        if downsized_image_size - (kernel_size-1) >= 1:
            downsized_image_size = downsized_image_size - (kernel_size-1)

        if downsized_image_size - (kernel_size-1) <= 1:
            random_kernel = random.randint(1, max([downsized_image_size, 1]))
            downsized_image_size = downsized_image_size - (random_kernel-1)
            temp_kernel = genes_utils.int_to_genes(random_kernel, genes.WIDTH_KERNEL)

        kernel.append(temp_kernel)
    return kernel

def generate_dropouts(dropouts, layers_amount):
    for _ in range(layers_amount):
        dropouts.append(genes.DEFAULT_DROPOUT_LAYER)

    dropout_amount = random.randint(0, layers_amount)
    layer_indexes = list(range(0, dropout_amount))
    for _ in range(dropout_amount):
        dropout_index = random.choice(layer_indexes)
        layer_indexes.remove(dropout_index)

        dropout_weight = genes_utils.int_to_genes(round(random.uniform(0,1)*100), len(genes.DEFAULT_DROPOUT_LAYER))
        dropout_remaining = [0 for i in range(7 - len(dropout_weight))]
        dropouts[dropout_index] = dropout_remaining + dropout_weight

def generate_layers(layers):
    for _ in range(genes.LAYERS):
        layers.append(random.randint(0, 1))
    validate_layers(layers)

def generate_nodes(layers, nodes, layers_amount):
    for i in range(layers_amount):
        temp_nodes = []
        for _ in range(genes.NODES):
            temp_nodes.append(random.randint(0, 1))
        nodes.append(temp_nodes)
    nodes = validate_nodes(layers=layers, nodes=nodes)