import os
import matplotlib.image as mpimg

def list_files(path):
    return [f for f in os.listdir(path) if not f.startswith('.')]

def dataset_dimensions(base_dir):
    classification_folder_name = list_files(os.path.join(base_dir, 'train'))[0]
    images_path = os.path.join(base_dir, 'train', classification_folder_name)
    image_name = list_files(images_path)[0]
    image_path = os.path.join(base_dir, 'train', classification_folder_name, image_name)
    image_shape = mpimg.imread(image_path).shape
    downsized_image_width = image_shape[0]
    downsized_image_height = image_shape[1]
    return (downsized_image_width, downsized_image_height)


