import unittest
import unittest.mock as mock
from random import choice
import random
from genetic import genes
from genetic.member import Member
from genetic.genetics import Genetics

class TestGenetics(unittest.TestCase):
    
    @mock.patch('genetic.genes.LAYERS', 3)
    @mock.patch('genetic.genes.NODES', 8)
    @mock.patch('genetic.utils.image_utils.dataset_dimensions', return_value=(32, 32))
    @mock.patch('genetic.utils.genetics_utils.generate_child_kernel', return_value=[])
    @mock.patch('genetic.utils.genetics_utils.generate_child_dropouts', return_value=[])
    @mock.patch('random.uniform', return_value=0.1)
    def test_crossover_parent_1(
        self,
        kernel_mock,
        image_dimensions_mock,
        dropouts_mock,
        uniform_mock):
        parent1 = Member(new_member=False, base_dir=None, classes=None)
        parent1.layers = [0, 0, 1]
        parent1.nodes = [
            [1, 0, 0, 0, 1, 0, 0, 0],
        ]

        parent1.dense_layers = [0, 1, 0]
        parent1.dense_nodes = [
            [0, 0, 0, 0, 1, 0, 0, 1],
            [1, 0, 0, 0, 1, 0, 0, 0]
        ]

        parent2 = Member(new_member=False, base_dir=None, classes=None)
        parent2.layers = [0, 1, 1]
        parent2.nodes = [
            [0, 0, 1, 0, 1, 0, 0, 0],
            [0, 0, 1, 0, 1, 0, 0, 0],
            [0, 0, 1, 0, 1, 0, 0, 0],
            
        ]
        parent2.dense_layers = [1, 0, 1]
        parent2.dense_nodes = [
            [0, 0, 0, 0, 1, 0, 0, 1],
            [0, 0, 0, 0, 1, 0, 0, 1],
            [0, 0, 0, 0, 1, 0, 0, 1],
            [0, 0, 0, 0, 1, 0, 0, 1],
            [1, 0, 0, 0, 1, 0, 0, 0]
        ]

        genetics = Genetics()
        child = genetics.crossover(parent1, parent2, classes=None)

        self.assertEqual(child.layers, parent1.layers)
        self.assertEqual(child.dense_layers, parent1.dense_layers)
        self.assertEqual(child.dense_nodes, parent1.dense_nodes)
        self.assertEqual(child.nodes, parent1.nodes)
        

    @mock.patch('genetic.genes.LAYERS', 3)
    @mock.patch('genetic.genes.NODES', 8)
    @mock.patch('genetic.utils.image_utils.dataset_dimensions', return_value=(32, 32))
    @mock.patch('genetic.utils.genetics_utils.generate_child_kernel', return_value=[])
    @mock.patch('genetic.utils.genetics_utils.generate_child_dropouts', return_value=[])
    @mock.patch('random.uniform', return_value=0.9)
    def test_crossover_parent_2(
        self,
        kernel_mock,
        image_dimensions_mock,
        dropouts_mock,
        uniform_mock):
        parent1 = Member(new_member=False, base_dir=None, classes=None)
        parent1.layers = [1, 1, 0]
        parent1.dense_layers = [0, 1, 0]
        parent1.dense_nodes = [
            [0, 0, 0, 0, 1, 0, 0, 0],
            [0, 0, 0, 0, 1, 0, 0, 0]
        ]
        parent1.nodes = [
            [ 1, 1, 1, 1, 0, 1, 1, 0 ],
        ]
        parent2 = Member(new_member=False, base_dir=None, classes=None)
        parent2.dense_layers = [0, 0, 1]
        parent2.dense_nodes = [
            [0, 0, 0, 0, 0, 0, 1, 1]
        ]
        parent2.layers = [0, 1, 0]
        parent2.nodes = [
            [ 0, 1, 1, 1, 1, 1, 1, 1 ],
            [ 1, 0, 1, 1, 1, 1, 1, 1],
        ]
        genetics = Genetics()
        child = genetics.crossover(parent1, parent2, classes=None)
        self.assertEqual(child.layers, parent2.layers)
        self.assertEqual(child.nodes, parent2.nodes)

        self.assertEqual(child.dense_layers, parent2.dense_layers)
        self.assertEqual(child.dense_nodes, parent2.dense_nodes)



    @mock.patch('genetic.genes.LAYERS', 3)
    @mock.patch('genetic.genes.NODES', 8)
    @mock.patch('genetic.utils.image_utils.dataset_dimensions', return_value=(32, 32))
    def test_crossover_seeded(self, image_dimensions_mock):

        random.seed(15378586)
        parent1 = Member(new_member=False, base_dir=None, classes=None)
        parent1.layers = [1, 1, 0]
        parent1.nodes = [
            [ 0, 1, 1, 1, 0, 1, 1, 0 ],
            [ 1, 0, 1, 1, 0, 1, 1, 0 ],
            [ 1, 1, 0, 1, 0, 1, 1, 0 ],
            [ 1, 1, 1, 0, 0, 1, 1, 0 ],
            [ 1, 1, 1, 1, 0, 1, 1, 0 ],
            [ 1, 1, 1, 1, 0, 0, 1, 0 ],
        ]

        parent1.kernel_height = [
            [1,1], [1,1], [0,1],
            [1,0], [1,0], [0,1],
        ]

        parent1.kernel_width = [
            [1,1], [1,1], [0,1],
            [1,1], [1,1], [0,1],
        ]

        parent1.dense_layers = [0, 0, 1]
        parent1.dense_nodes = [[0, 1, 0, 0, 0, 0, 0, 0]]
        
        parent1.dropouts = [
            [0, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0],
        ]
        
        parent2 = Member(new_member=False, base_dir=None, classes=None)
        parent2.layers = [0, 0, 1]
        parent2.nodes = [
            [ 0, 1, 1, 1, 1, 1, 1, 0 ],
        ]
        parent2.dense_layers = [1, 0, 0]
        parent2.dense_nodes = [
            [0, 0, 0, 1, 1, 0, 0, 0],
            [0, 1, 0, 0, 1, 0, 0, 0],
            [0, 1, 0, 0, 1, 0, 0, 0],
            [0, 0, 1, 0, 1, 0, 0, 0]
        ]

        parent2.dropouts = [
            [0, 0, 1, 0, 1, 0, 0],
        ]

        parent2.kernel_width = [ [0,1] ]
        parent2.kernel_height = [ [1,1] ]

        genetics = Genetics()
        child = genetics.crossover(parent1, parent2, classes=None)

        self.assertEqual(child.layers, [0, 1, 0])

        self.assertEqual(child.nodes, [[0, 1, 1, 1, 1, 1, 1, 0], [0, 1, 1, 1, 1, 1, 1, 0]])

        self.assertEqual(child.dropouts, [[0, 0, 1, 0, 1, 0, 0], [0, 1, 1, 0, 1, 0, 0]])

        self.assertEqual(child.dense_layers, [1, 0, 1])
        self.assertEqual(child.dense_nodes,
            [
                [0, 1, 0, 1, 1, 0, 0, 0], 
                [0, 1, 0, 0, 0, 0, 0, 0], 
                [0, 1, 0, 0, 0, 0, 0, 0], 
                [0, 1, 1, 0, 0, 0, 0, 0], 
                [0, 1, 0, 1, 1, 0, 0, 0]
            ]
        )

        self.assertEqual(child.kernel_height, [[1, 1], [1, 1]])
        self.assertEqual(child.kernel_width, [[1, 1], [0, 1]])

