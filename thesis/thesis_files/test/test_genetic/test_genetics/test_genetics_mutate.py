import unittest
from genetic.genetics import Genetics
import unittest.mock as mock
import random
from genetic.member import Member

class TestGenetics(unittest.TestCase):

    @mock.patch('genetic.genes.LAYERS', 3)
    @mock.patch('genetic.genes.NODES', 8)
    @mock.patch('genetic.utils.image_utils.dataset_dimensions', return_value=(32, 32))
    def test_mutate_seeded(self, image_dimensions_mock):
        random.seed(15378586)
        child = Member(new_member=False, base_dir=None, classes=None)
        child.dense_layers = [1, 0, 0]
        child.dense_nodes = [
            [0, 0, 0, 1, 1, 0, 0, 0],
            [0, 1, 0, 0, 1, 0, 0, 0],
            [0, 1, 0, 0, 1, 0, 0, 0],
            [0, 0, 1, 0, 1, 0, 0, 0]
        ]

        child.dropouts = [
            [0, 0, 1, 0, 1, 0, 0],
        ]

        child.kernel_width = [ [0,1] ]
        child.kernel_height = [ [1,1] ]

        child.layers = [1, 1, 0]
        child.nodes = [[1, 1, 1, 1, 1, 1, 1, 0], [1, 1, 1, 1, 1, 1, 1, 0], [1, 1, 1, 1, 1, 1, 1, 0]]

        genetics = Genetics()
        genetics.mutate(child)

        self.assertEqual(child.layers, [1, 1, 0])
        self.assertEqual(child.nodes, [[1, 1, 1, 1, 1, 1, 1, 0], [1, 1, 1, 1, 1, 1, 1, 0], [1, 1, 1, 1, 1, 1, 1, 0]])
        self.assertEqual(child.kernel_width, [[0, 1]])
        self.assertEqual(child.kernel_height, [[1, 1]])
        self.assertEqual(child.dropouts, [[0, 0, 1, 0, 1, 0, 0]])
        self.assertEqual(child.dense_layers, [1, 0, 0])
        self.assertEqual(child.dense_nodes, [[0, 0, 0, 1, 1, 0, 0, 0], [0, 1, 0, 0, 1, 0, 0, 0], [0, 1, 0, 0, 1, 0, 0, 0], [0, 0, 1, 0, 1, 0, 0, 0]])

if __name__ == '__main__':
    unittest.main()