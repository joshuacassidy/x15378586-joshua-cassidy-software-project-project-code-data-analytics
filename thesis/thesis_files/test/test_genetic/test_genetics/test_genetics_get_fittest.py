import unittest
from genetic.genetics import Genetics
import unittest.mock as mock
import random
from genetic.member import Member
from genetic.population import Population

class TestGenetics(unittest.TestCase):

    @mock.patch('genetic.genes.LAYERS', 3)
    @mock.patch('genetic.genes.NODES', 8)
    def test_get_fittest(self):
        random.seed(15378586)
        population = Population(10, False, "/", None)
        for i in range(len(population.members)):
            population.members[i].fitness = i
        genetics = Genetics()
        member = genetics.get_fittest(population)
        self.assertEqual(member.fitness, 8)
