import unittest
import unittest.mock as mock
from random import choice
from genetic import genes
from genetic.utils import net_generator
from genetic.member import Member
from genetic.genetics import Genetics
import matplotlib.image

class TestGenetics(unittest.TestCase):

    def test_validate_kernel(self):
        self.assertEqual([0, 1], net_generator.validate_kernel([0,0]))
        self.assertEqual([1, 0], net_generator.validate_kernel([1,0]))
    
    def test_validate_layers(self):
        self.assertEqual([0, 0, 1], net_generator.validate_layers([0, 0, 0]))
        self.assertEqual([1, 1, 0], net_generator.validate_layers([1, 1, 0]))

    def test_validate_nodes(self):
        self.assertEqual(net_generator.validate_nodes([0,0], [[0,0]]), [[0, 1]])
        self.assertEqual(net_generator.validate_nodes([1,0,1], [[1,1]]), [[1, 1]])

        

        