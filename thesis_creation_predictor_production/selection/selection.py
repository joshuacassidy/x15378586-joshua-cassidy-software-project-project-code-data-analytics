import pymongo,  pickle


client = pymongo.MongoClient("mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/",authSource="thesis")
db = client.thesis


results = db.best_generated_networks.find({ "$and": [ { "labels": {"$ne": None} }, { "acc": { "$gte": 0.7 } } ] } , sort=[('acc', -1)])



data = []

labels_dic = {}


for record in results:
    try:
        if '/' in record['dataset']:
            record['dataset'] = record['dataset'].split("/")[-1]
        if record['dataset'] not in ['cifar100', 'cifar10', 'mnist', 'fashion_mnist']:
            continue
        record['labels'].sort()

        hashed_labels = ''.join(record['labels'])
        if hashed_labels in labels_dic:
            continue

        labels_dic[hashed_labels] = hashed_labels
        
        filtered_record = {
            "_id": record['_id'],
            "nodes": record['nodes'],
            "layers": record['layers'],
            "dense_nodes": record['dense_nodes'],
            "dense_layers": record['dense_layers'],
            "dropouts": record['dropouts'],
            "dataset": record['dataset'],
            "kernel_width": record['kernel_width'],
            "kernel_height": record['kernel_height'],
            "labels": record['labels'],
            "acc": record['acc'],
            "loss": record['loss'],
            "generation": record['generation'],
            "compile_time": record['compile_time'],
            "fitness": record['fitness'],
        }
        data.append(filtered_record)
        
    except:
        pass

with open("select_data/network_arch.pkl", "wb") as mypicklefile:
    pickle.dump(data, mypicklefile)
