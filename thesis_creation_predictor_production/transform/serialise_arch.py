import thesis_model_serialisation.network_representation as network_representation
import os 

import pickle

with open("select_data/network_arch.pkl", "rb") as mypicklefile:
    data = pickle.load(mypicklefile)


new_data = []

for rec in range(len(data)):
    serailised_net = network_representation.serialise(data[rec])
    data[rec]['layers_bytes'] = serailised_net['layers_bytes']
    data[rec]['dense_layers_bytes'] = serailised_net['dense_layers_bytes']
    data[rec]['nodes_bytes'] = serailised_net['nodes_bytes']
    data[rec]['dense_nodes_bytes'] = serailised_net['dense_nodes_bytes']
    data[rec]['kernel_width_bytes'] = serailised_net['kernel_width_bytes']
    data[rec]['kernel_height_bytes'] = serailised_net['kernel_height_bytes']
    data[rec]['dropouts_bytes'] = serailised_net['dropouts_bytes']
    data[rec]['model_representation'] = serailised_net['model_representation']
    new_data.append(data[rec])

with open("transform_data/network_arch.pkl", "wb") as mypicklefile:
    pickle.dump(new_data, mypicklefile)