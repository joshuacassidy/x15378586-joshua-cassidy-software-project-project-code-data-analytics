import io, zipfile, time, os, pickle

with open("transform_data/network_arch.pkl", "rb") as mypicklefile:
    data = pickle.load(mypicklefile)


dataset_size = 0
compressed_dataset_size = 0
images_amount = 0


for i in range(len(data)):
    memory_file = io.BytesIO()
    with zipfile.ZipFile(memory_file, 'w', zipfile.ZIP_DEFLATED) as zf:

        labels_amount = len(data[i]['labels'])
        for label in data[i]['labels']:
            if label.startswith('.'):
                continue
            images_path = os.path.join(data[i]['dataset'], 'train', label)
            images = os.listdir(images_path)
            for image in images:
                zf.write(os.path.join(images_path, image), image)
                images_amount += 1
        for info in zf.infolist():
            dataset_size += info.file_size
            compressed_dataset_size += info.compress_size
    data[i]['dataset_size'] = dataset_size
    data[i]['compressed_dataset_size'] = compressed_dataset_size
    data[i]['labels_amount'] = labels_amount
    data[i]['image_dimensions'] = data[i]['mean_red'].shape
    data[i]['images_amount'] = images_amount



with open("transform_data/network_arch.pkl", "wb") as mypicklefile:
    pickle.dump(data, mypicklefile)
