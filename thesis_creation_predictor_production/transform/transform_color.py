import pickle
import numpy as np


dataset_data = {}

with open("select_data/cifar100_images.pkl", "rb") as mypicklefile:
    cifar100_images = pickle.load(mypicklefile)
    dataset_data.update(cifar100_images)

with open("select_data/fashion_mnist_images.pkl", "rb") as mypicklefile:
    fashion_mnist_images = pickle.load(mypicklefile)
    dataset_data.update(fashion_mnist_images)

with open("select_data/mnist_images.pkl", "rb") as mypicklefile:
    mnist_images = pickle.load(mypicklefile)
    dataset_data.update(mnist_images)

with open("select_data/cifar10_images.pkl", "rb") as mypicklefile:
    cifar10_images = pickle.load(mypicklefile)
    dataset_data.update(cifar10_images)


with open("transform_data/network_arch.pkl", "rb") as mypicklefile:
    net_data = pickle.load(mypicklefile)

for i in net_data:
    current_data = list(dataset_data[i['labels'][0]])
    for label in range(1, len(i['labels'])):
        for record in dataset_data[i['labels'][label]]:
            current_data.append(record)
    
    arr = np.mean(current_data, axis=0)
    i['mean_red'] = arr[:,:,0]
    i['mean_green'] = arr[:,:,1]
    i['mean_blue'] = arr[:,:,2]

   

with open("transform_data/network_arch.pkl", "wb") as mypicklefile:
    pickle.dump(net_data, mypicklefile)


