import pickle
import numpy as np

with open("transform_data/network_arch.pkl", "rb") as mypicklefile:
    data = pickle.load(mypicklefile)

metadata = []
net_data_x = []
net_data_y = []

def get_min_max_array(data, column):
    col_max = None
    col_min = None
    for i in range(len(data)):
        col_min_cur = np.min(data[i][column])
        col_max_cur = np.max(data[i][column])

        if col_max is None or col_max_cur > col_max:
            col_max = col_max_cur

        if col_min is None or col_min_cur < col_min:
            col_min = col_min_cur
    return (col_max, col_min)


def get_min_max(data, column, index=None):
    col_max = None
    col_min = None
    for i in range(len(data)):
        col_min_cur = data[i][column]
        col_max_cur = data[i][column]
        if index is not None:
            col_min_cur = col_min_cur[index]
            col_max_cur = col_max_cur[index]
            

        if col_max is None or col_max_cur > col_max:
            col_max = col_max_cur

        if col_min is None or col_min_cur < col_min:
            col_min = col_min_cur
    return (col_max, col_min)

def get_mean_std(data, column, index=None):
    selected_data = []
    
    for i in range(len(data)):
        if index is None:
            if isinstance(data[i][column], np.ndarray):
                for j in data[i][column].copy().flatten():
                    selected_data.append(j)
            else:
                selected_data.append(i)
        else:
            selected_data.append(i)
    standard_deviation = np.std(selected_data)
    mean = np.mean(selected_data)
    return (standard_deviation, mean)


edges_std_rec, edges_mean_rec = get_mean_std(data, "edges")
dataset_size_std_rec, dataset_size_mean_rec = get_mean_std(data, "dataset_size")
compressed_dataset_size_std_rec, compressed_dataset_size_mean_rec = get_mean_std(data, "compressed_dataset_size")
labels_amount_std_rec, labels_amount_mean_rec = get_mean_std(data, "labels_amount")
width_dimensions_std_rec, width_dimensions_mean_rec = get_mean_std(data, "image_dimensions", 0)
height_dimensions_std_rec, height_dimensions_mean_rec = get_mean_std(data, "image_dimensions", 1)
images_amount_std_rec, images_amount_mean_rec = get_mean_std(data, "images_amount")
mean_red_std_rec, mean_red_mean_rec = get_mean_std(data, "mean_red")
mean_green_std_rec, mean_green_mean_rec = get_mean_std(data, "mean_green")
mean_blue_std_rec, mean_blue_mean_rec = get_mean_std(data, "mean_blue")

for i in range(len(data)):
    data[i]['edges'] = (data[i]['edges'] - edges_mean_rec) / edges_std_rec
    record = np.concatenate(
        [
            ((data[i]['mean_red'] - mean_red_mean_rec) / mean_red_std_rec),
            ((data[i]['mean_green'] - mean_green_mean_rec) / mean_green_std_rec),
            ((data[i]['mean_blue'] - mean_blue_mean_rec) / mean_blue_std_rec),
            data[i]['edges']
            
        ]
    )

    data[i]['dataset_size'] = (data[i]['dataset_size'] - dataset_size_mean_rec) / (dataset_size_std_rec)
    data[i]['compressed_dataset_size'] = (data[i]['compressed_dataset_size'] - compressed_dataset_size_mean_rec) / (compressed_dataset_size_std_rec)
    data[i]['labels_amount'] = (data[i]['labels_amount'] - labels_amount_mean_rec) / (labels_amount_std_rec)
    data[i]['images_amount'] = (data[i]['images_amount'] - images_amount_mean_rec) / (images_amount_std_rec)
    width = (data[i]['image_dimensions'][0] - width_dimensions_mean_rec) / (width_dimensions_std_rec)
    height = (data[i]['image_dimensions'][1] - height_dimensions_mean_rec) / (height_dimensions_std_rec)
    
    metadata_record = np.array(
        [
            data[i]['dataset_size'],
            data[i]['compressed_dataset_size'],
            data[i]['labels_amount'],
            data[i]['images_amount'],
            width,
            height,
        ]
    )
    
    metadata.append(metadata_record)

    net_data_x.append(record)
    net_data_y.append(data[i]['model_representation'])
    

net_data_x = np.array(net_data_x)
net_data_y = np.array(net_data_y)
metadata = np.array(metadata)

with open("transform_data/raw_training_data.pkl", "wb") as mypicklefile:
    pickle.dump(net_data_x, mypicklefile)

with open("transform_data/raw_meta_training_data.pkl", "wb") as mypicklefile:
    pickle.dump(metadata, mypicklefile)



with open("model_data/labels.pkl", "wb") as mypicklefile:
    pickle.dump(net_data_y, mypicklefile)
