import pickle
import numpy as np


with open("transform_data/raw_training_data.pkl", "rb") as mypicklefile:
    data = pickle.load(mypicklefile)

size = 500

new_data = []

for i in data:
    record = list(i)
    pooling_window_size = len(record) / 500
    pooling_window_remainder = len(record) % 500
    pooled_record = []
    for j in range(0, size):
        current_window = record[j*int(pooling_window_size): (j * int(pooling_window_size)) + int(pooling_window_size)]
        pooled_record.append(max(current_window))
    
    current_window = record[(j * int(pooling_window_size)) + int(pooling_window_size): ]
    pooled_record.append(max(current_window))
    new_data.append(np.array(pooled_record))
    
new_data = np.array(new_data)


with open("transform_data/raw_meta_training_data.pkl", "rb") as mypicklefile:
    metadata = pickle.load(mypicklefile)


new_data = np.concatenate([new_data, metadata], axis=1)


with open("model_data/data.pkl", "wb") as mypicklefile:
    pickle.dump(new_data, mypicklefile)
