import pickle
from skimage import filters


dataset_data = {}

with open("select_data/cifar100_images_gray.pkl", "rb") as mypicklefile:
    cifar100_images = pickle.load(mypicklefile)
    dataset_data.update(cifar100_images)

with open("select_data/fashion_mnist_images_gray.pkl", "rb") as mypicklefile:
    fashion_mnist_images = pickle.load(mypicklefile)
    dataset_data.update(fashion_mnist_images)

with open("select_data/mnist_images_gray.pkl", "rb") as mypicklefile:
    mnist_images = pickle.load(mypicklefile)
    dataset_data.update(mnist_images)

with open("select_data/cifar10_images_gray.pkl", "rb") as mypicklefile:
    cifar10_images = pickle.load(mypicklefile)
    dataset_data.update(cifar10_images)


edge_data = {}

for key in dataset_data.keys():
    current_data = list(dataset_data[key])
    merged_data = filters.sobel(current_data[0])
    for i in range(1, len(current_data)):
        merged_data += filters.sobel(current_data[i])
    edge_data[key] = merged_data
    edge_data[key+"_count"] = len(dataset_data[key])


with open("transform_data/edges.pkl", "wb") as mypicklefile:
    pickle.dump(edge_data, mypicklefile)

