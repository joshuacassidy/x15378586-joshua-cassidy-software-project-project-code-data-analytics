BSc (Honours) in Computing Data Analytics
Joshua Cassidy - X15378586

NOTE THE GITLAB REPOSITORY HAS MORE OF THE DATA FILES THAT THE GOOGLE DRIVE LINK.
GitLab Link: https://gitlab.com/joshuacassidy/x15378586-joshua-cassidy-software-project-project-code-data-analytics
Google Drive Link: https://drive.google.com/open?id=1xWY4sBARUFEwrGarCVc9vgBXtynU_Meo
 
 
The projects UI can be accessed at: http://87.44.4.253:8080/
Please make sure that the project is running on the machine and the machine is available. The machine can be accessed by using running the command ssh joshua@tesla.cloudenci.ie and using the password in Appendix C. The code is in the thesis_production folder and can be run by entering the folder and running the command: waitress-serve --call 'web_server:create_app'.

The credentials of the database and the web server are provided in Appendix C of the documentation submission.

System Components
- Sub-system 1
- Sub-system 2
- Sub-system 3
- Network Serialisation Package
- Structure Data Package
- Model Predictor Data Generation Package
- Model Generator Data Selection Package
- Data Extraction Package
- Parse Predictor Data Package
- Predictor Data Extraction Package
- Web Server

Sub-system 1 is contained in the thesis/thesis_files folder, thesis_system_1_data_generation/model_predictor_data_generation/final_year_project_thesis and in the thesis_production/final_year_project_thesis folders. Sub-system was developed in the thesis/thesis_files folder.
Sub-system 2 is contained in the thesis_production/predictor_network folder
Sub-system 3 is contained in the thesis_system_3 folder [Please ensure that the note books are connected to the data files when viewing the visualisations]
Network Serialisation Package is contained in the thesis_production/thesis_model_serialisation folder.
Structure data package is contained in the thesis_production/thesis_structure_data folder
Model Predictor Data Generation Package is contained in the thesis_system_1_data_generation folder
Model Generator Data Selection Package is contained in the model_generation_data_selection folder
Data Extraction Package is contained in the thesis_extract_data folder
Parse Predictor Data Package is contained in the thesis_production/clean_uploaded_dataset folder
Predictor Data Extraction Package is contained in the thesis_creation_predictor_production
Web Server is contained in the thesis_production folder


Project Dependancies (Note the specific version of all the Python dependencies can be found in requirements.txt):
1. Tableau
2. Python3 (with python3 set as an enviorement variable required to run the jobs in the web server)
3. Graphviz (when generating images from the downloaded instructions)
4. NumPy
5. TensorFlow
6. Keras
7. PyMongo
8. Sci-kit learn
9. cv2
10. Flask
11. PyNonpar

The database data folder contains the database data in the event that the database gets wiped.
The regression_testing_scripts folder contains the GIT hooks files that were used to perform regression testing.
The python_regression_testing_script was used to test Subsystem 1, and the network serialisation package.
The web_server_regression_testing_script was used to test the Web server executing the tests for the Sub-system 2, the Web servers unit tests and end to end tests.
The regression scripts were place in the hooks folder of the relevant projects and were renamed to pre-commit, and the command chmod +x precommit command was run in the hooks folder to allow the hooks files to be executed. Note as this code sits in the one repository for the purposes of the upload the git repository files had to be deleted in each component of the project as to not cause upload conflicts.

Model Predictor Data Generation Package
1. Run the "run.sh" bash script from the thesis_system_1_data_generation directory

Predictor Data Extraction Package
1. Run the "run.sh" bash script from the thesis_creation_predictor_production directory
2. Ensure that the full extracted versions of the mnist, fashion_mnist, cifar10, and cifar100 datasets are in this directory. (with both the structured and unstructed folders)
3. Upon Completion insert the generated models in to the models folder in the root director of the thesis_production folder, also insert the network_arch.pkl file into training_data directory of the thesis_production folder

Web Server (Note the python3 variable should be set in the enviroment variables for the Web server to run correctly)
1. Ensure that you are in the thesis_production
2. Ensure that the models are in the models directory and the network_arch.pkl file is in the training_data directory created by the Predictor Data Extraction Package
3. Run the command waitress-serve --call 'web_server:create_app' to start the Web Server


Running Sub-system 3:
1. Ensure you are in the Sub-system 3 directory
2. Export the data from the database to JSON files by running the command python3 extract.py
3. Tableau can then be used to view the pre-made visualisations (ensure Tableau is connected to the correct JSON file) visualisations can also be made using Tableau and the exported data

The statistical_analysis folder contains the code files for performing the Mann-Whitey U tests, Shapiro-Wilk Tests and generating the histograms comparing the systems losses, accuracies and compile times. The statistical_analysis.py file also generates the CSV files that were used with SPSS to generate graphs and statistical statistics. [Note that results produced by this script may vary in the even more data is inserted into the database]

To run project to generate all of the data:
1. Run the Model Predictor Data Generation Package
2. Run the Predictor Data Extraction Package (ensure the extracted imagesets are copied into this projects run directory)
3. Run the web server (Ensure the generated models are inserted to the models folder in the root director of the thesis_production folder, also insert the network_arch.pkl file into training_data directory of the thesis_production folder created by the Predictor Data Extraction Package). The statistical tests can also be run at this point

Note: The models generated by the Model Predictor Data Generation Package have been moved to the models folder in the thesis_production as this is where is where they are required to be for the web server to run correctly.



To run the project in order:
1.	Download the project's codebase from the projects Git Repository https://gitlab.com/joshuacassidy/x15378586-joshua-cassidy-software-project-project-code-data-analytics 
2.	Open up a new terminal instance and change directory into the thesis_system_1_data_generation folder and run the run.sh file to run the Data Extraction, Model Generator Data Selection, and Model Predictor Data Generation Packages
3.	Then change the directory of the terminal into the thesis_creation_predictor_production directory and run the run.sh file to run the Predictor Data Extraction Package. Ensure that the raw image datasets are in the root folder of the thesis_creation_predictor_production folder. This will generate the models to be used by the sub-system 2 and will be stored in the model's folder upon generation.
4.	Move the files from the thesis_creation_predictor_production model's folder into the model's folder in the root directory of the thesis_production folder. Additionally move network_arch.pkl file is in the transform data folder (thesis_creation_predictor_production/transform_data) to the training_data directory in the thesis_production created by the Predictor Data Extraction package.
5.	Run the command waitress-serve --call 'web_server:create_app' to start the Web Server (note the web server can be started when downloaded from the GitLab repository as it has the required files).
6.	To run sub-system 3: 
    a.	Ensure you are in the Sub-system 3 directory 2. 
    b.	Export the data from the database to JSON files by running the command python3 extract.py.
    c.	Tableau can then be used to view the pre-made visualisations (ensure Tableau is connected to the correct JSON file) visualisations can also be made using Tableau and the exported data.
NOTE: The project setup requires a significant amount of computation resources and time to set up due to the nature of the project.


To only extract the raw image datasets run the following commands in the thesis_system_1_data_generation folder:
    - python3 thesis_extract_data/extract_cifar100.py
    - python3 thesis_extract_data/extract_cifar10.py
    - python3 thesis_extract_data/extract_mnist.py
    - python3 thesis_extract_data/extract_fashion_mnist.py


The Model Generator test suite can be executed by using the command python3 -m unittest test when the user is in thesis/thesis_files folder
The network serialisation suite can be executed using the same command when the user is in the directory for the Network Serialisation package folder. (i.e in the thesis_production/thesis_model_serialisation folder)

The Model Predictor test suite, web server web API test suite and web server test suite can be executed together by using the command python3 -m unittest test when the user is in the thesis_production folder. 
The Web Server User Interfaces Test Suite can be executed by using the command mvn -f thesis_end_to_end/ test when the user is in the thesis_production folder.
Note: Ensure that the development web server is running when these tests are executed. To start the development web server, run the following command in the root directory of the project using another terminal window: waitress-serve --call 'web_server:create_app'.
