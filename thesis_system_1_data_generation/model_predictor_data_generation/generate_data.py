import pymongo, json
from bson.json_util import dumps, loads
import final_year_project_thesis.app as app

client = pymongo.MongoClient("mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/?compressors=disabled&gssapiServiceName=mongodb",authSource="thesis")

for i in client["thesis"]['predictor_training_data'].find({}):
    app.generate_networks(i['dataset_name'], i['labels'], early_stopping=True)
    client["thesis"]['predictor_training_data'].remove({'labels': i['labels']})

client.close()




