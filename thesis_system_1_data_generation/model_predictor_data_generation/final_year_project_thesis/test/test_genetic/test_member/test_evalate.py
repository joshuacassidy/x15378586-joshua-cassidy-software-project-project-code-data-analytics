import unittest, random
import genetic.utils.genetics_utils as genetics_utils
from genetic.member import Member
import genetic.utils.genes_utils as genes_utils
from genetic import genes
import unittest.mock as mock


class Member_Mock:
    def generate_network_code(self):
        return """
def create_neural_net(base_dir, train_dir, validation_dir, test_dir, classes_categories, patience = 2, early_stopping='False'):
    return (0.1, 0.95) 
        """

class TestGenetics(unittest.TestCase):

    @mock.patch('genetic.member.Member.generate_network_code', new=Member_Mock.generate_network_code)
    def test_evaluation(self):
        member = Member(new_member=False, base_dir="/", classes=None)
        self.assertEqual(member.evaluation(), 32130.82395833418)