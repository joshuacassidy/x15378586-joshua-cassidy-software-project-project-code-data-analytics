import unittest
import unittest.mock as mock
from random import choice
import random
from genetic import genes
from genetic.member import Member
from genetic.genetics import Genetics
import matplotlib.image

class Member_Mock:
    def evaluation(self):
        pass

class TestGenetics(unittest.TestCase):

    @mock.patch('genetic.genes.LAYERS', 3)
    @mock.patch('genetic.genes.NODES', 8)
    @mock.patch('genetic.genes.HEIGHT_KERNEL', 2)
    @mock.patch('genetic.genes.WIDTH_KERNEL', 2)
    @mock.patch('genetic.genes.DEFAULT_DROPOUT_LAYER', [0, 0, 0, 0, 0, 0, 0])
    @mock.patch('genetic.utils.image_utils.dataset_dimensions', return_value=(8, 8))
    @mock.patch('genetic.member.Member.evaluation', new=Member_Mock.evaluation)
    def test_generate(self, image_dimensions_mock):
        random.seed(15378586)
        member = Member(new_member=False, base_dir=None, classes=None)
        member.generate()

        layers=[0, 0, 1] 
        nodes=[[1, 1, 0, 0, 0, 0, 0, 1]]
        kernel_width=[[0, 1]]
        kernel_height=[[1, 1]]
        dropouts=[[0, 1, 0, 0, 0, 0, 0]]
        dense_layers=[1, 0, 0]
        dense_nodes=[[0, 1, 1, 1, 1, 1, 0, 0], [0, 1, 0, 1, 0, 0, 0, 1], [1, 1, 0, 0, 1, 1, 1, 1], [0, 0, 1, 1, 0, 1, 1, 0]]

        self.assertEqual(layers, member.layers)
        self.assertEqual(nodes, member.nodes)
        self.assertEqual(kernel_width, member.kernel_width)
        self.assertEqual(kernel_height, member.kernel_height)
        self.assertEqual(dropouts, member.dropouts)
        self.assertEqual(dense_layers, member.dense_layers)
        self.assertEqual(dense_nodes, member.dense_nodes)