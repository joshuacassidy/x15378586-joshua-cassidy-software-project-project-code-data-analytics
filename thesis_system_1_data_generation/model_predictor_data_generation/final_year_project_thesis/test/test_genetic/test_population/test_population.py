import unittest
import unittest.mock as mock
import random
from genetic.population import Population
from genetic.member import Member


class Member_Mock:

    def generate(self):
        self.acc = 0.95

class TestGenetics(unittest.TestCase):

    @mock.patch('genetic.genes.LAYERS', 3)
    @mock.patch('genetic.genes.NODES', 8)
    def test_get_fittest(self):
        random.seed(15378586)
        population = Population(10, False, "/", None)
        for i in range(len(population.members)):
            population.members[i].fitness = i
        member = population.get_fittest_member()
        self.assertEqual(member.fitness, 9)

    

    @mock.patch('genetic.member.Member.generate', new=Member_Mock.generate)
    @mock.patch('genetic.genes.LAYERS', 3)
    @mock.patch('genetic.genes.NODES', 8)
    def test___init__(self):
        random.seed(15378586)
        population = Population(10, False, "/", None)
        self.assertEqual(len(population.members), 10)

        population = Population(10, True, "/", None)
        self.assertEqual(len(population.members), 10)

        population = Population(10, True, "/", None, early_stopping=True)
        self.assertEqual(len(population.members), 3)



