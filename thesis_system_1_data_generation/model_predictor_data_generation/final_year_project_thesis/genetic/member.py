import random, math
from . import genes
import datetime
import os, shutil
import tensorflow as tf
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

from keras import layers
from keras import models
from keras.utils import to_categorical
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import keras
from keras.preprocessing.image import ImageDataGenerator
from .utils import net_generator
from .utils import genes_utils
from .utils import image_utils

class Member:
    def __init__(self, new_member, base_dir, classes, early_stopping=False):
        self.loss = 0
        self.acc = 0
        self.compile_time = 0
        self.early_stopping = early_stopping
        self.classes = classes
        self.layers = []
        self.nodes = []
        self.kernel_width = []
        self.kernel_height = []
        self.dropouts = []
        self.dense_layers = []
        self.dense_nodes = []
        self.fitness = 0
        self.prob = 0
        self.base_dir = base_dir
        if new_member == True:
            self.generate()
        else:
            pass

    def generate(self):
        downsized_image_width, downsized_image_height = image_utils.dataset_dimensions(self.base_dir)

        net_generator.generate_layers(self.layers)
        net_generator.generate_layers(self.dense_layers)
        layers_value = genes_utils.genes_to_int(self.layers)
        dense_layers_amount = genes_utils.genes_to_int(self.dense_layers)
        
        net_generator.generate_dropouts(self.dropouts, layers_value)

        self.kernel_width = net_generator.generate_kernel(layers_value, downsized_image_width)
        self.kernel_height = net_generator.generate_kernel(layers_value, downsized_image_height)
        net_generator.generate_nodes(layers=self.layers, nodes=self.nodes, layers_amount=layers_value)
        
        net_generator.generate_nodes(
            layers=self.dense_layers, 
            nodes=self.dense_nodes, 
            layers_amount=dense_layers_amount
        )
        
        self.evaluation()

    def generate_network_code(self):
        from . import net_template
        layers = ""
        dense_layers = ""
        layers_template = "model.add(Conv2D({nodes}, ({kernel_width}, {kernel_height}), activation='relu'))"
        dropout_template = "model.add(Dropout({weight}))\n    "
        dense_layer_template = "model.add(Dense({nodes}, activation='relu'))\n    "

        layers_value = genes_utils.genes_to_int(self.layers)
        
        for i in range(0, layers_value):
            if i != 0:
                layers += "    "
            nodes_value = genes_utils.genes_to_int(self.nodes[i])
            kernel_width = genes_utils.genes_to_int(self.kernel_width[i])
            kernel_height = genes_utils.genes_to_int(self.kernel_height[i])
            if self.dropouts[i] != genes.DEFAULT_DROPOUT_LAYER:
                weight = genes_utils.genes_to_int(self.dropouts[i])/100
                if weight > 1:
                    weight = 1
                layers += dropout_template.format(weight=weight)
            layers += layers_template.format(nodes=nodes_value, kernel_width=kernel_width, kernel_height=kernel_height) + "\n"

        dense_layers_amount = genes_utils.genes_to_int(self.dense_layers)

        for i in range(dense_layers_amount):
            nodes_value = genes_utils.genes_to_int(self.dense_nodes[i])
            dense_layers += dense_layer_template.format(nodes=nodes_value)
        
        network_code = net_template.code.format(layers=layers, dense_layers=dense_layers)
        
        return network_code

    def evaluation(self):
        network_code = self.generate_network_code()

        start = datetime.datetime.now()
        train_dir = os.path.join(self.base_dir, 'train')
        validation_dir = os.path.join(self.base_dir, 'validation')
        test_dir = os.path.join(self.base_dir, 'test')

        execute_net_code = """create_neural_net(
            base_dir="{base_dir}", 
            train_dir="{train_dir}", 
            validation_dir="{validation_dir}", 
            test_dir="{test_dir}",
            classes_categories="{classes_categories}",
            early_stopping="{early_stopping}"
        )""".format(
            base_dir=self.base_dir, 
            train_dir=train_dir, 
            validation_dir=validation_dir,
            test_dir=test_dir,
            classes_categories=(','.join(self.classes) if self.classes is not None else 'null' ),
            early_stopping=str(self.early_stopping)
        )
        
        context = {}

        exec(network_code + "\ntest_loss, test_acc = " + execute_net_code, context)
        end = datetime.datetime.now()

        self.loss = context['test_loss']
        self.acc = context['test_acc']
        self.compile_time = end - start
        self.fitness = math.exp(math.exp(math.exp(self.acc - self.loss)))


        return self.fitness
    
    def __str__(self):
        return """Member(
                    layers={layers} 
                    nodes={nodes}, 
                    fitness={fitness}, 
                    loss={loss}, 
                    acc={acc},
                    compile_time={compile_time},
                    kernel_width={kernel_width},
                    kernel_height={kernel_height},
                    dropouts={dropouts},
                    dense_layers={dense_layers},
                    dense_nodes={dense_nodes},
                )""".format(
                    layers=str(self.layers),
                    nodes=str(self.nodes),
                    fitness=str(self.fitness),
                    loss=str(self.loss),
                    acc=str(self.acc),
                    compile_time=str(self.compile_time),
                    kernel_width=str(self.kernel_width),
                    kernel_height=str(self.kernel_height),
                    dropouts=str(self.dropouts),
                    dense_layers=str(self.dense_layers),
                    dense_nodes=str(self.dense_nodes),
                )


