import os, random, math,sys
import pymongo
random.seed(15378586)

def combinations(n,r):
    f = math.factorial
    return f(n) / f(r) / f(n-r)


datasets_size = 3000
subset = []
attributes = os.listdir("cifar100_unstructured")

temp_arr = []
for i in attributes:
    if not i.startswith('.'):
        temp_arr.append(i)
attributes = temp_arr

for amount in range(2, 5):
    i = 0
    while i < (min([datasets_size, int(combinations(len(attributes),amount))])):
        temp_subset = []
        j = 0
        while j < amount:
            choice = random.choice(attributes)
            if choice in temp_subset or choice.startswith('.'):
                pass
            else:
                j += 1
                temp_subset.append(choice)
        temp_subset.sort()
        if temp_subset in subset:
            pass
        else:
            i+=1
            subset.append(temp_subset)

client = pymongo.MongoClient("mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/?compressors=disabled&gssapiServiceName=mongodb",authSource="thesis")
db = client.thesis

db.predictor_training_data.remove({})

for i in subset:
    record = {
        "dataset_name": "cifar100", 
        "labels": i
    }
    db.predictor_training_data.insert_one(record)
client.close()