from keras.datasets import fashion_mnist
import os
import extract_utils

(x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

fashion_label_names = {
    0: "t_shirt", 
    1: "trouser",
    2: "pullover",
    3: "dress",
    4: "coat",
    5: "sandal",
    6: "shirt",
    7: "sneaker",
    8: "bag",
    9: "ankle_boot",
}

output_dir = 'fashion_mnist_unstructured'

extract_utils.create_unstructured_directory(fashion_label_names, output_dir, x_train, y_train, x_test, y_test)

extract_utils.create_structured_directory('fashion_mnist')

dataset_source_directory = 'fashion_mnist_unstructured'
labels = os.listdir(dataset_source_directory)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='fashion_mnist', 
    directory_type='test', 
    start_image_amount=0,
    images_amount=1000
)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='fashion_mnist', 
    directory_type='validation', 
    start_image_amount=1000,
)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='fashion_mnist', 
    directory_type='train', 
    start_image_amount=1000,
)

