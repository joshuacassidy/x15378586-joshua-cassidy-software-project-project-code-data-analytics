from keras.datasets import cifar10
import os
import extract_utils

(x_train, y_train), (x_test, y_test) = cifar10.load_data()

cifar10_label_names = {0: 'airplane', 1: 'automobile', 2: 'bird', 3: 'cat', 4: 'deer', 5: 'dog', 6: 'frog', 7: 'horse', 8: 'ship', 9: 'truck'}

output_dir = 'cifar10_unstructured'

extract_utils.create_unstructured_directory(cifar10_label_names, output_dir, x_train, y_train, x_test, y_test)

extract_utils.create_structured_directory('cifar10')

dataset_source_directory = 'cifar10_unstructured'
labels = os.listdir(dataset_source_directory)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='cifar10', 
    directory_type='test', 
    start_image_amount=0,
    images_amount=1000
)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='cifar10', 
    directory_type='validation', 
    start_image_amount=1000,
)

extract_utils.populate_directory(
    dataset_source_directory=dataset_source_directory, 
    output_dir='cifar10', 
    directory_type='train', 
    start_image_amount=1000,
)




