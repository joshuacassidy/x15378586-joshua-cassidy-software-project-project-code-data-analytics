import os, shutil
import numpy as np
from PIL import Image, ImageOps

def save_image(filename, data_array):
    im = Image.fromarray(data_array.astype('uint8'))
    ImageOps.invert(im).save(filename)

# y_train[i][0]

def create_unstructured_directory(label_names, output_dir, x_train, y_train, x_test, y_test):
    try:
        shutil.rmtree(output_dir)
    except:
        pass
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    for i in range(len(x_train)):
        if isinstance(y_train[i], np.ndarray) and not isinstance(y_train[i], np.uint8):
            label_name = label_names[y_train[i][0]]
        else:
            label_name = label_names[y_train[i]]
        folder_name = os.path.join(output_dir, label_name)
        if not os.path.exists(folder_name):
            os.mkdir(folder_name)

    for i in range(0, len(y_train)):
        if isinstance(y_train[i], np.ndarray) and not isinstance(y_train[i], np.uint8):
            label_name = label_names[y_train[i][0]]
        else:
            label_name = label_names[y_train[i]]
        filename = '{i}.jpg'.format(i=i)
        file_url = os.path.join(output_dir, label_name, filename)
        save_image(file_url, x_train[i])

    for i in range(0, len(y_test)):
        if isinstance(y_test[i], np.ndarray) and not isinstance(y_test[i], np.uint8):
            label_name = label_names[y_test[i][0]]
        else:
            label_name = label_names[y_test[i]]
        filename = '{i}.jpg'.format(i=(i+len(y_train)))
        file_url = os.path.join(output_dir, label_name, filename)
        save_image(file_url, x_test[i])


def create_structured_directory(output_dir):
    try:
        shutil.rmtree(output_dir)
    except:
        pass
    os.mkdir(output_dir)
    test_directory = 'test'
    train_directory = 'train'
    validation_directory = 'validation'
    directories = [train_directory, test_directory, validation_directory]
    for i in directories:
        os.mkdir(os.path.join(output_dir, i))


def populate_directory(dataset_source_directory, output_dir, directory_type, start_image_amount, images_amount=None):
    use_all = False
    if images_amount is None:
        use_all = True
    labels = os.listdir(dataset_source_directory)
    for label in labels:
        if label.startswith('.'):
            continue
        images = os.listdir(os.path.join(dataset_source_directory, label))
        try:
            os.mkdir(os.path.join(output_dir, directory_type, label))
        except:
            pass
        if use_all:
            images_amount = len(images)
        print(images_amount)
        print(label)
        for j in range(start_image_amount, images_amount):
            source_location = os.path.join(dataset_source_directory, label, images[j])
            destination_location = os.path.join(output_dir, directory_type, label, images[j])
            shutil.copyfile(source_location, destination_location)
